set(CMAKE_HOST_SYSTEM "Linux-5.3.0-45-generic")
set(CMAKE_HOST_SYSTEM_NAME "Linux")
set(CMAKE_HOST_SYSTEM_VERSION "5.3.0-45-generic")
set(CMAKE_HOST_SYSTEM_PROCESSOR "x86_64")

include("/opt/development/zynq/Xilinx/Workspace.new/EchoTestFreeRTA91_bsp/ps7_cortexa9_1/libsrc/libmetal_v2_0/src/libmetal/cmake/platforms/toolchain.cmake")

set(CMAKE_SYSTEM "FreeRTOS")
set(CMAKE_SYSTEM_NAME "FreeRTOS")
set(CMAKE_SYSTEM_VERSION "")
set(CMAKE_SYSTEM_PROCESSOR "arm")

set(CMAKE_CROSSCOMPILING "TRUE")

set(CMAKE_SYSTEM_LOADED 1)
