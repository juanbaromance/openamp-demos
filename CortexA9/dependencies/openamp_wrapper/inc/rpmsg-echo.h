#ifndef RPMSG_ECHO_H
#define RPMSG_ECHO_H

#define RPMSG_SERVICE_NAME         "rpmsg-openamp-demo-channel"

#ifdef __cplusplus
extern "C" {
#endif

    int startup(void);
    
#ifdef __cplusplus
}
#endif

#endif /* RPMSG_ECHO_H */
