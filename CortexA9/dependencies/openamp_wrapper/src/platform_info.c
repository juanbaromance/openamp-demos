/*
 * Copyright (c) 2014, Mentor Graphics Corporation. All rights reserved.
 * Copyright (c) 2017 - 2018 Xilinx, Inc. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

/**************************************************************************
 * FILE NAME
 *
 *       platform_info.c
 *
 * DESCRIPTION
 *
 *       This file define platform specific data and implements APIs to set
 *       platform specific information for OpenAMP.
 *
 **************************************************************************/

#include <errno.h>
#include <metal/atomic.h>
#include <metal/device.h>
#include <metal/io.h>
#include <metal/irq.h>
#include <metal/sys.h>
#include "openamp_wrapper/platform_info.h"
#include "openamp_wrapper/platform.public.h"
#include "openamp_wrapper/rsc_table.h"
#include <xparameters.h>

/* Another APU core ID. In this demo, the other APU core is 0. */
#define A9_CPU_ID	0UL

/* scugic device, used to raise soft irq */
#define SCUGIC_DEV_NAME	"scugic_dev"
#define SCUGIC_BUS_NAME	"generic"

/* scugic base address */
#define SCUGIC_PERIPH_BASE	0xF8F00000
#define SCUGIC_DIST_BASE	(SCUGIC_PERIPH_BASE + 0x00001000)

#define _rproc_wait() asm volatile("wfi")
typedef struct remoteproc remoteproc;
static remoteproc *platform_create_proc(int proc_index, int rsc_index);

#ifndef nullptr
#define nullptr NULL
#endif

extern int init_system(void);
int platform_init(int argc, char *argv[], void **platform)
{
    static const char *backtrace = __FUNCTION__ ;
    xil_printf("%s: startup\n", backtrace );
    if ( platform == nullptr ) {
        xil_printf("%s: Failed to initialize platform," "NULL pointer to store platform data.\n" , backtrace );
        return -EINVAL;
    }
    /* Initialize HW system components */
    init_system();
    unsigned long proc_id = ( argc >= 2 ) ? strtoul(argv[1], NULL, 0) : 0;
    unsigned long rsc_id  = ( argc >= 3 ) ? strtoul(argv[2], NULL, 0) : 0 ;

    if ( ( *platform = platform_create_proc( proc_id, rsc_id ) ) == nullptr ) {
        xil_printf("%s: Failed to create remoteproc device.\n", backtrace );
        return -EINVAL;
    }
    return 0;
}

/* Remoteproc instance and private data */
static remoteproc rproc;
static struct remoteproc_priv rproc_priv = {
    .gic_name = SCUGIC_DEV_NAME,
    .gic_bus_name = SCUGIC_BUS_NAME,
    .gic_dev = NULL,
    .gic_io = NULL,
    .irq_to_notify = SGI_TO_NOTIFY,
    .irq_notification = SGI_NOTIFICATION,
    /* toggle CPU ID, there is only two CPUs in PS */
    .cpu_id = (~XPAR_CPU_ID) & ZYNQ_CPU_ID_MASK,
};
typedef struct remoteproc_ops remoteproc_ops;
/* processor operations for hil_proc for A9. It defines
 * notification operation and remote processor management. */
static metal_phys_addr_t scugic_phys_addr = SCUGIC_DIST_BASE;
struct metal_device scugic_device = {
    .name = SCUGIC_DEV_NAME,
    .bus = NULL,
    .num_regions = 1,
    .regions = {
        {
            .virt = (void *)SCUGIC_DIST_BASE,
            .physmap = &scugic_phys_addr,
            .size = 0x1000,
            .page_shift = -1UL,
            .page_mask = -1UL,
            .mem_flags = DEVICE_MEMORY,
            .ops = {NULL},
            },
        },
    .node = {NULL},
    .irq_num = 0,
    .irq_info = NULL,
};

typedef struct remoteproc remoteproc;
static remoteproc *platform_create_proc( int proc_index, int rsc_index )
{
	(void) proc_index;
    /* Register GIC device to access SGI area */
    (void)metal_register_generic_device( & scugic_device );
    extern remoteproc_ops zynq_a9_proc_ops;
    if ( remoteproc_init( &rproc, & zynq_a9_proc_ops, & rproc_priv) == nullptr )
        return nullptr;

    int rsc_size;
    void *rsc_table = get_resource_table(rsc_index, & rsc_size );
    metal_phys_addr_t tmp = (metal_phys_addr_t)( rsc_table );
    (void *)remoteproc_mmap( &rproc, &tmp, NULL, rsc_size, NORM_NONCACHE | STRONG_ORDERED,&rproc.rsc_io);
    tmp = SHARED_MEM_PA;
    (void *)remoteproc_mmap( &rproc, &tmp, NULL, SHARED_MEM_SIZE,NORM_NONCACHE | STRONG_ORDERED,NULL);

    static const char *backtrace = __FUNCTION__ ;
    int ret = remoteproc_set_rsc_table( & rproc, (struct resource_table *)rsc_table, rsc_size);
	if (ret) {
        xil_printf("%s: Failed to intialize remoteproc\n", backtrace );
        remoteproc_remove( & rproc );
		return NULL;
	}
    xil_printf("%s: success\n", backtrace );
    return &rproc;
}

/* RPMsg virtio shared buffer pool */
static struct rpmsg_virtio_shm_pool shpool;
typedef struct rpmsg_virtio_device rpmsg_virtio_device;
typedef struct virtio_device virtio_device;
rpmsg_device* platform_create_vdev(void *platform, unsigned int vdev_index, enum VirtIORole role, reset_cb reset_cb, operation_cb operation_cb )
{
    rpmsg_virtio_device *rpmsg_vdev = (rpmsg_virtio_device *)metal_allocate_memory(sizeof(rpmsg_virtio_device));
    if ( rpmsg_vdev == nullptr )
        return NULL;

    remoteproc *rproc = (remoteproc*)platform;
    struct metal_io_region *shbuf_io = remoteproc_get_io_with_pa(rproc, SHARED_MEM_PA);
    if ( shbuf_io == nullptr )
        return NULL;

    void *shbuf = metal_io_phys_to_virt(shbuf_io, SHARED_MEM_PA + SHARED_BUF_OFFSET);
    static const char *backtrace = __FUNCTION__ ;
    xil_printf("%s: %s-virtio maps @ phy.$%08x(%5dKbytes)\n",
               backtrace, role == VIRTIO_DEV_MASTER ? "Master" : "Slave", SHARED_MEM_PA, SHARED_MEM_SIZE >> 10 );
    virtio_device *vdev = ( virtio_device*)remoteproc_create_virtio(rproc, vdev_index, role, reset_cb);
    if ( vdev == nullptr ) {
        xil_printf("%s: failed remoteproc_create_virtio\n", backtrace );
        goto err1;
    }

    xil_printf("%s: initializing rpmsg vdev\n", backtrace );
    int ret;
    if (role == VIRTIO_DEV_MASTER) {
        rpmsg_virtio_init_shm_pool(&shpool, shbuf,(SHARED_MEM_SIZE - SHARED_BUF_OFFSET));
        ret =  rpmsg_init_vdev(rpmsg_vdev, vdev, operation_cb, shbuf_io, &shpool);
    } else {
        ret =  rpmsg_init_vdev(rpmsg_vdev, vdev, operation_cb,shbuf_io, NULL);
    }
    if (ret) {
        xil_printf("%s: failed rpmsg_init_vdev\n", backtrace );
        goto err2;
    }
    xil_printf("%s: initializing rpmsg vdev\n", backtrace );
    return rpmsg_virtio_get_rpmsg_device( rpmsg_vdev );

err2:
    remoteproc_remove_virtio(rproc, vdev);
err1:
    metal_free_memory(rpmsg_vdev);
    return nullptr;
}

rpmsg_endpoint* platform_create_ept( rpmsg_device *rdev, const char *ept_name, uint32_t start, uint32_t end, epoint_operation epo, epoint_reset epr )
{
    rpmsg_endpoint* ept = ( rpmsg_endpoint* )metal_allocate_memory( sizeof( rpmsg_endpoint ) );
    if( ept )
        if( rpmsg_create_ept( ept, rdev, ept_name, start, end, epo, epr ) != 0 )
            return 0;
    return ept;
}

int platform_xmitt_ept(rpmsg_endpoint *ept, void *payload, size_t len )
{ return rpmsg_send(ept, payload, len) < 0 ? -1 : RPMSG_SUCCESS; }

typedef struct remoteproc_priv remoteproc_priv;
int platform_poll(void *priv)
{
    remoteproc *rproc = ( remoteproc *)priv;
    unsigned int flags;

    remoteproc_priv *prproc = (remoteproc_priv *)rproc->priv;
    while( 1 ) {
		flags = metal_irq_save_disable();
        if ( (atomic_flag_test_and_set( & prproc->nokick) == 0) )
        {
            metal_irq_restore_enable( flags );
#ifdef VERBOSE
            xil_printf("%s: Wait Remote kick", __FUNCTION__ );
#endif
			remoteproc_get_notification(rproc, RSC_NOTIFY_ID_ANY);
			break;
		}
		_rproc_wait();
		metal_irq_restore_enable(flags);
	}
	return 0;
}

void platform_release_vdev(rpmsg_device *rpdev, rpmsg_endpoint *epoint )
{
    rpmsg_destroy_ept( epoint );
    metal_free_memory( epoint );
    (void)rpdev;
}

extern void cleanup_system(void);
void platform_cleanup(void *platform)
{
    remoteproc *rproc = ( remoteproc* ) platform;
    if (rproc)
        remoteproc_remove(rproc);
    cleanup_system();
}
