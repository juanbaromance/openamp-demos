/*
 * Copyright (c) 2018 Xilinx, Inc. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

/**************************************************************************
 * FILE NAME
 *
 *       zynq_a9_rproc.c
 *
 * DESCRIPTION
 *
 *       This file define Xilinx Zynq A9 platform specific remoteproc
 *       implementation.
 *
 **************************************************************************/

#include <openamp/remoteproc.h>
#include <metal/atomic.h>
#include <metal/device.h>
#include <metal/irq.h>
#include "openamp_wrapper/platform_info.h"
#include <bsp/xil_printf.h>
#include <bsp/xparameters.h>
#include <bsp/xparameters_ps.h>
/* SCUGIC macros */
#define GIC_SFI_TRIG_SATT_MASK             0x00008000
#define GIC_DIST_SOFTINT                   0xF00
#define GIC_CPU_ID_BASE                    (1 << 4)

#ifndef nullptr
#define nullptr NULL
#endif

typedef struct remoteproc remoteproc;
typedef struct remoteproc_ops remoteproc_ops;
typedef struct remoteproc_priv remoteproc_priv;
static remoteproc *zynq_a9_rproc_init( remoteproc *rproc, remoteproc_ops *ops, void *arg );
static void *zynq_a9_rproc_mmap   ( remoteproc *rproc, metal_phys_addr_t *pa, metal_phys_addr_t *da, size_t size, unsigned int attribute, struct metal_io_region **io);
static void  zynq_a9_rproc_remove ( remoteproc *rproc);
static int   zynq_a9_rproc_notify ( remoteproc *rproc, uint32_t id);

/* processor operations from r5 to a53. It defines
 * notification operation and remote processor managementi operations. */
struct remoteproc_ops zynq_a9_proc_ops = {
    .init   = zynq_a9_rproc_init,
    .remove = zynq_a9_rproc_remove,
    .mmap   = zynq_a9_rproc_mmap,
    .handle_rsc = nullptr,
    .config = nullptr,
    .start = nullptr,
    .stop = nullptr,
    .shutdown = nullptr,
    .notify = zynq_a9_rproc_notify,
};

static int zynq_a9_proc_irq_handler( int vect_id, void *data )
{
    (void)vect_id;
    if ( data == nullptr )
        return METAL_IRQ_NOT_HANDLED;

    remoteproc_priv *prproc = (remoteproc_priv* )( ( remoteproc* )data )->priv;
    atomic_flag_clear( & prproc->nokick );
	return METAL_IRQ_HANDLED;
}

static remoteproc *zynq_a9_rproc_init( remoteproc *rproc, struct remoteproc_ops *ops, void *arg)
{
    remoteproc_priv *prproc = (remoteproc_priv*)arg;

	if (!rproc || !prproc || !ops)
		return NULL;

    struct metal_device *dev;
    int ret = metal_device_open( prproc->gic_bus_name, prproc->gic_name, & dev );
    if ( ret ) {
        xil_printf("open gic-device %s : failure(%d)\r\n", prproc->gic_name, ret );
        return nullptr;
	}

    if( ( prproc->gic_io = metal_device_io_region( prproc->gic_dev = dev, 0) ) == nullptr )
    {
        xil_printf("open gic-device %s mapping: failure(%d)\r\n", prproc->gic_name, ret );
        metal_device_close(dev);
        return nullptr;
    }

    rproc->priv = prproc;
    atomic_flag_test_and_set( & prproc->nokick );
    rproc->ops = ops;
    unsigned int irq = prproc->irq_notification;
    metal_irq_register( irq, zynq_a9_proc_irq_handler, rproc );
    metal_irq_enable( irq );
    xil_printf("%s: metallize success\n\tslave.peer(aka myself) CPU#%02d %s.phy(0x%08x).irq%03d(0x%02x)\n\t"
               "master.peer target CPU#%02d.irq%03d(0x%02x)\n\n",
               __FUNCTION__ ,
               XPAR_CPU_ID,
               prproc->gic_name,
               metal_io_phys( prproc->gic_io, 0 ),
               irq, irq,
               prproc->cpu_id, prproc->irq_to_notify, prproc->irq_to_notify );
	return rproc;

}

static void zynq_a9_rproc_remove( remoteproc *rproc )
{
    if ( rproc )
        if( rproc->priv )
        {
            remoteproc_priv *prproc = ( remoteproc_priv *)rproc->priv;
            metal_irq_disable( prproc->irq_to_notify );
            metal_irq_unregister( prproc->irq_to_notify );
            metal_device_close( prproc->gic_dev );
        }
}

typedef struct remoteproc_mem remoteproc_mem;
typedef struct metal_io_region metal_io_region;
static void *zynq_a9_rproc_mmap( remoteproc *rproc, metal_phys_addr_t *pa, metal_phys_addr_t *da, size_t size, unsigned int attribute, struct metal_io_region **io)
{
    metal_phys_addr_t lpa = *pa, lda = *da;

	if (lpa == METAL_BAD_PHYS && lda == METAL_BAD_PHYS)
        return nullptr;

	if (lpa == METAL_BAD_PHYS)
		lpa = lda;
	if (lda == METAL_BAD_PHYS)
		lda = lpa;

    if ( ! attribute )
		attribute = NORM_NONCACHE | STRONG_ORDERED;

    remoteproc_mem *mem = (remoteproc_mem*)metal_allocate_memory(sizeof(*mem));
	if (!mem)
		return NULL;

    metal_io_region *tmpio = (metal_io_region*)metal_allocate_memory(sizeof(*tmpio));
	if (!tmpio) {
		metal_free_memory(mem);
		return NULL;
	}
	remoteproc_init_mem(mem, NULL, lpa, lda, size, tmpio);
	/* va is the same as pa in this platform */
	metal_io_init(tmpio, (void *)lpa, &mem->pa, size,sizeof(metal_phys_addr_t)<<3, attribute, NULL);
	remoteproc_add_mem(rproc, mem);
	*pa = lpa;
	*da = lda;
	if (io)
		*io = tmpio;

    xil_printf("%s: metalling $%08x(%6d Kbytes)(%s)\n", __FUNCTION__ , mem->pa, size >> 10, io ? "io" : "mem" );
    return metal_io_phys_to_virt( tmpio, mem->pa );
}

#define GIC_SFI_TRIG_CPU_MASK              0x00FF0000
#define GIC_SFI_TRIG_INTID_MASK            0x0000000F
#define KICK_MASK (GIC_SFI_TRIG_CPU_MASK | GIC_SFI_TRIG_INTID_MASK)
static int zynq_a9_rproc_notify( remoteproc *rproc, uint32_t id )
{
    if ( rproc == nullptr )
		return -1;
    remoteproc_priv *prproc = ( remoteproc_priv* )rproc->priv;
    if (prproc->gic_io == nullptr)
        return -1;

    unsigned long kick = ( 1 << ( GIC_CPU_ID_BASE + prproc->cpu_id ) ) | prproc->irq_to_notify ;
    kick &= KICK_MASK;

#ifdef VERBOSE
    xil_printf("%s(%d:0x%08x): kick.val( $%08x )\n", __FUNCTION__ , id, id, kick );
#endif

    metal_io_write32( prproc->gic_io, GIC_DIST_SOFTINT, kick );
	return 0;
}


