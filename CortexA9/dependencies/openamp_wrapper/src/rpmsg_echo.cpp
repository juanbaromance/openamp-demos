#include <bitset>
#include "openamp_wrapper/platform.public.h"
#include "bsp/xil_printf.h"
#include "bsp/bspconfig.h"
#include <cassert>

#ifdef FREERTOS_BSP
#include "bsp/FreeRTOS.h"
#include "bsp/task.h"

#else

#include "modules/mockup.h"

#endif

static constexpr const char* channel_name = "rpmsg-openamp-demo-channel";
static constexpr uint32_t Shutdown = 0xEF56A55A;

class cOpenAMPClass {
    using cClass = cOpenAMPClass;

public:
    cOpenAMPClass()
    {
        csr = 0;
        TaskHandle_t comm_task{nullptr};
        xTaskCreate( trampoline, "bypass" , 1024, this, 2, & comm_task );
    }

private:

    static std::bitset<4> csr;
    static void trampoline( void *args ){ reinterpret_cast<cClass*>(args)->deployment(); }

    void deployment()
    {
        csr.set(Online);
        void *platform;
        if( int err; ( err = platform_init( 0, nullptr, & platform ) == 0 ) )
        {
            rpmsg_device *rpdev = platform_create_vdev( platform, 0, VirtiIOSlave, nullptr, nullptr );
            if( rpdev )
            {
                if( rpmsg_endpoint *endpoint = platform_create_ept( rpdev, channel_name, 0, 0xFFFFFFFF, epoint_trampoline, epoint_reset ) )
                {
                    while( csr.test(Online) )
                    {
                        platform_poll( platform );
                    }
                    dump( __PRETTY_FUNCTION__ + std::to_string(0) );
                    platform_release_vdev(rpdev, endpoint );
                }
                else
                {
                    dump( __PRETTY_FUNCTION__ + std::string(": failure instancing endpoint-msg" ));
                }
            }
            else
            {
                dump( __PRETTY_FUNCTION__ + std::string(": failure instancing virtio-dev" ));
                assert(false);
            }
            platform_cleanup( platform );
        }
        else
        {
            dump( __PRETTY_FUNCTION__  + std::string(": failure instancing plaftorm"));
        }
    }
    enum {
        Online = 0
    };

    static void dump( const std::string & backtrace ){ xil_printf( "%s\n", backtrace.c_str() ); }

    static int epoint_trampoline( rpmsg_endpoint *ept, void *data, size_t len, uint32_t src, void *priv )
    { return reinterpret_cast<cOpenAMPClass*>(priv)->epoint_callback(ept, data, len, src ); }
    int epoint_callback( rpmsg_endpoint *ept, void *data, size_t len, uint32_t src )
    {
        if ( uint32_t tmp = ( *(unsigned int *)data ); tmp == Shutdown )
        {
            static constexpr const char* backtrace = __FUNCTION__ ;
            dump( std::string( backtrace ) + ": shutdown message received" );
            csr.reset(Online);
            return 0;
        }
        return platform_xmitt_ept( ept, data, len );
    }

    static void epoint_reset( struct rpmsg_endpoint* )
    {
        static constexpr const char* backtrace = __FUNCTION__ ;
        dump( backtrace );
        csr.reset(Online);
    }

};

std::bitset<4> cOpenAMPClass::csr;
extern "C" { void startup(){ cOpenAMPClass(); } }
