#include "mockup.h"
#include <cstring>

void vTaskDelete(TaskHandle_t handle)
{

}

void vTaskStartScheduler()
{

}

BaseType_t xTaskCreate( callback cb, const char *name, size_t stack_size, void *user_arg, int priority, TaskHandle_t *handle)
{
    if( strcmp(name,"bypass") == 0 )
    {
        cb(user_arg);
    }
    return pdPASS;
}

long xTaskGetTickCount()
{
    return 0;
}

void xTimerStop(TimerHandle_t, uint32_t delay)
{

}

void xTimerStart(TimerHandle_t, uint32_t delay)
{

}

void *pvTimerGetTimerID(TimerHandle_t)
{
    return nullptr;
}

TimerHandle_t xTimerCreate(const char *name, const TickType_t elapsed, int activity, void *user_arg, timer_callback trampoline)
{
    return nullptr;
}

void configASSERT(TimerHandle_t)
{

}

void xTaskNotify(TaskHandle_t, size_t trigger, eOperation operation)
{

}

BaseType_t xTaskNotifyWait(int unknown, size_t max, uint32_t *notification, uint32_t deadline)
{
    return pdPASS;
}
