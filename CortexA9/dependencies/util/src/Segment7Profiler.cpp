#include "Segment7Profiler.h"
#include "util.h"
#include <cassert>
#include <iosfwd>

//static S7Profiler probe;

S7Profiler::S7Profiler() : Module<S7Profiler>(""), jerk_factor( default_jerk )
{
    error = maskGenerator(AccOObounds,VelOOBounds,UnConfig);
    bounds.settled = maskGenerator(AccOObounds,VelOOBounds);

//    setAccLimit(300);
//    setVelLimit(150);
//    generator( 12*M_PI, 1.8 );
//    assert(0);

}

S7Profiler & S7Profiler::setLimit( size_t limit )
{
    bounds.settled.reset(limit);
    if( bounds.settled.none() )
        error.reset(UnConfig);
    return *this;
}

S7Profiler& S7Profiler::setAccLimit( double acc )
{
    bounds.acc = acc;
    return setLimit( AccOObounds );
}

S7Profiler& S7Profiler::setVelLimit( double vel )
{
    bounds.vel = vel;
    return setLimit(VelOOBounds);
}

S7Profiler& S7Profiler::setJerkRatio( double jerk_factor )
{
    this->jerk_factor = jerk_factor > default_jerk ? default_jerk : jerk_factor ;
    return *this;
}

void S7Profiler::report( double target, const std::string & backtrace ) const
{
    using namespace std;
    stringstream oss;
    oss << "# " << backtrace << "err(0b" << error << ")" << " sec using jerk-factor " << jerk_factor << endl;
    oss << fixed << setprecision(1);
    oss << "# Target : " << target << " deadlined " << deadline <<" sec\n";
    oss << "# Delta : " << this->distance << "\n";
    oss << "# Cruise phase : " << velocity << " mm/sec " << ( velocity/bounds.vel )*100 << "%\n";
    oss << "# Acceleration phase : " << t_acc << " sec " << acc << " mm/sec2 " << ( acc/bounds.acc )*100 << "%\n";
    oss << "# Jerk phase : " << t_jerk << " sec " << jerk << " mm/sec3\n";
    auditor( oss );
}

bool S7Profiler::beyondLimits( double target, double orig, double deadline )
{
    using namespace std;
    if( error.test(UnConfig) )
    {
        std::stringstream oss;
        oss << "settings not fullfilled : 0b" << bounds.settled;
        return auditor( oss, Warning );
    }

    double distance = abs( target - orig );
    acc = distance / ( jerk_factor * ( 1 - jerk_factor) * ( 1 - phi ) * deadline * deadline );
    velocity = distance / ( deadline * ( 1 - jerk_factor ) );
    t_acc = deadline  * jerk_factor;
    t_jerk = t_acc / 4;
    jerk = acc / t_jerk;

    error.set( AccOObounds, acc > bounds.acc );
    error.set( VelOOBounds, velocity > bounds.vel );
    this->distance = distance;
    this->deadline = deadline;
    if( error.any() )
        report( target );
    return error.any();
}

using seg0 = std::tuple<double,double,double,double>;
class segment {
private:
    enum
    {
        EndOfSegment,
        Time,
        Position,
        Velocity
    };
    seg0 s0;

public:
    segment( const seg0 & s0_ = seg0() ) : s0( s0_ ) { }
    double t0()  const { return std::get<Time>(s0); }
    double p0()  const { return std::get<Position>(s0); }
    double v0()  const { return std::get<Velocity>(s0); }
    double eos() const { return std::get<EndOfSegment>(s0); }

};

#include <variant>
#include "bsp/xil_printf.h"

bool S7Profiler::generator( double target, double deadline, Buffer buffer, double p1, double v1 )
{    
    using namespace std;

    if( beyondLimits( target, p1, deadline ) )
        return false;

    Buffer dummy = []( const struct point & point )
    {
        std::stringstream oss;
        oss << __FUNCTION__ << point.t << " " << point.p << " " << point.v << " " << point.a << " " << point.j << "\n";
        xil_printf( "%s", oss.str().c_str() );
    };
    if ( buffer == nullptr )
        buffer = dummy;

    std::stringstream oss;
    oss << __PRETTY_FUNCTION__ ;
    auditor( oss );

    double SAC = t_acc - ( 2 * t_jerk ); // constant acceleration segment
    double SAL = t_jerk; // linear acceleration segment
    double T1 = SAL, T2 = T1 + SAC, T3 = T2 + SAL;
    double T4 = t_acc + (( 1 - ( 2 * jerk_factor )) * deadline );
    double T5 = T4 + SAL, T6 = T5 + SAC;

    struct s1 : segment { s1( const seg0 & s ) : segment( s ) {} };
    struct s2 : segment { s2( const seg0 & s ) : segment( s ) {} };
    struct s3 : segment { s3( const seg0 & s ) : segment( s ) {} };
    struct s4 : segment { s4( const seg0 & s ) : segment( s ) {} };
    struct s5 : segment { s5( const seg0 & s ) : segment( s ) {} };
    struct s6 : segment { s6( const seg0 & s ) : segment( s ) {} };
    struct s7 : segment { s7( const seg0 & s ) : segment( s ) {} };
    using phase = variant<s1,s2,s3,s4,s5,s6,s7>;

    phase s = s1( { T1, 0, p1, v1 } );

    double seg_time{};
    double t = 0;
    double a_t{}, v_t{}, p_t{}, jerk_t{};
    double backward = target > p1 ? 1 : -1;

    while( t < deadline )
    {
        t += step_msec;
        seg_time = match( s, [&]( const auto & tmp ) -> double { return t - tmp.t0(); } );

        match ( s,
            [&]( const s1 & tmp )
            {
                jerk_t = jerk;
                a_t = jerk * seg_time;
                v_t = tmp.v0() + jerk * seg_time * seg_time /2 ;
                p_t = tmp.p0() +  backward * ( ( tmp.v0() * seg_time ) + ( jerk * seg_time * seg_time * seg_time /6 ) );
                if( t >=  tmp.eos() )
                    s = s2( { T2, t, p_t, v_t } );
            },

            [&]( const s2 & tmp )
            {
                jerk_t = 0;
                a_t = acc;
                v_t = v1 + ( acc * seg_time ) + ( jerk * SAL * SAL /2 );
                p_t =  tmp.p0() + backward * ( ( tmp.v0() * seg_time ) + ( acc * seg_time * seg_time /2 ) );
                if( t >= tmp.eos() )
                    s = s3( { T3, t, p_t, v_t });
            },

            [&]( const s3 & tmp )
            {
                jerk_t = - jerk;
                a_t = acc - ( jerk * seg_time );
                v_t = v1
                      + ( jerk * SAL * SAL /2 )
                      + ( acc  * SAC )
                      + ( acc  * seg_time )
                      - ( jerk * seg_time * seg_time /2 );
                p_t = tmp.p0() + backward * ( ( tmp.v0() * seg_time ) + ( acc * seg_time * seg_time /2) - ( jerk * seg_time * seg_time * seg_time /6 ) );
                if( t >= tmp.eos() )
                    s = s4( { T4, t, p_t, v_t } );
            },

            [&]( const s4 & tmp )
            {
                a_t = 0;
                jerk_t = 0;
                v_t = velocity;
                p_t = tmp.p0() + backward * ( tmp.v0() * seg_time );
                if( t >= tmp.eos() )
                    s = s5({ T5, t, p_t, v_t } );
            },

            [&]( const s5 & tmp )
            {
                jerk_t = - jerk;
                a_t = - jerk * seg_time;
                v_t = velocity - ( jerk * seg_time * seg_time /2 );
                p_t = tmp.p0() + backward * ( ( tmp.v0() * seg_time ) - ( jerk * seg_time * seg_time * seg_time /6 ) );
                if( t >= tmp.eos() )
                    s = s6( { T6, t, p_t, v_t });
            },

            [&]( const s6 & tmp )
            {
                jerk_t = 0;
                a_t = -acc;
                v_t = velocity - ( jerk * SAL * SAL /2  ) - ( acc * seg_time );
                p_t = tmp.p0() + backward * ( ( tmp.v0() * seg_time ) - ( acc * seg_time * seg_time /2 ) );
                if( t >= tmp.eos() )
                    s = s7({ deadline, t, p_t, v_t } );
            },

            [&]( const s7 & tmp )
            {
                jerk_t = jerk;
                a_t = - acc + ( jerk_t * seg_time );
                v_t = velocity
                      - ( jerk * SAL * SAL * 0.5 )
                      - ( acc  * SAC )
                      - ( acc  * seg_time )
                      + ( jerk * seg_time *seg_time /2 );
                p_t = tmp.p0() + backward * ( ( tmp.v0() * seg_time ) - ( acc * seg_time * seg_time / 2 ) + ( jerk * seg_time * seg_time * seg_time /6 ) );
            }
        );
        buffer( { t, p_t, v_t, a_t, jerk_t } );
    }
    return true;
}
