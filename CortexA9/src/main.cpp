#include "bsp/xil_printf.h"
#include "bsp/xil_assert.h"
#include "module.h"
#include "FreeRTOS.h"
#include "task.h"

#define auditor(format, ...) xil_printf(format, ##__VA_ARGS__)

#ifdef __cplusplus
extern "C" {
#endif

// void QemuTarget(){};
void assert( int condition ){ Xil_AssertVoid( condition ); }

#ifdef __cplusplus
}
#endif

int main()
{
    auditor("%s\n", __PRETTY_FUNCTION__ );

    deploy();
    vTaskStartScheduler();
    while (1) ;
    
    auditor("%s : ends\n", __PRETTY_FUNCTION__ );
    return 0;
}
