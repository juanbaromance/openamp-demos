
set ( CMAKE_SYSTEM_PROCESSOR "arm"            CACHE STRING "")
set ( MACHINE                "zynq7"          CACHE STRING "")
set ( CROSS_PREFIX           "arm-none-eabi-" CACHE STRING "")
set ( CMAKE_C_FLAGS          "-mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard -fmessage-length=0" CACHE STRING "")
set ( CMAKE_CXX_FLAGS        "-mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard -fmessage-length=0" CACHE STRING "")

set ( pwd ${CMAKE_CURRENT_SOURCE_DIR} )
set( Platform bmCa9 )
ADD_DEFINITIONS( -D${Platform}__ -DBareMetal__ -DUSE_AMP=1 )
include_directories(
  ${pwd}
  ./inc
  ./dependencies/include
  ./dependencies/include/bsp
)

cmake_policy(SET CMP0048 NEW)
# set ( CMAKE_GENERATOR make )
set ( CMAKE_SYSTEM_NAME "Generic"             CACHE STRING "")
set ( CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER CACHE STRING "")
set ( CMAKE_C_COMPILER   ${CROSS_PREFIX}gcc )
set ( CMAKE_CXX_COMPILER ${CROSS_PREFIX}g++ )

SET (CMAKE_C_COMPILER_WORKS 1)
SET (CMAKE_CXX_COMPILER_WORKS 1)

string ( CONCAT prelnk_spec
    "-Wl,-build-id=none "
    "-specs=${pwd}/Xilinx.spec -Wl,-T -Wl,${pwd}/lscript.ld " ${Target_deps} )

set( CMAKE_EXE_LINKER_FLAGS ${prelnk_spec} )

#target_include_directories( ${Target}
#    PUBLIC
#    ./dependencies/include/c++-8.2.0
#    ./dependencies/include/c++-8.2.0/arm-none-eabi
#    ./dependencies/include/gnu-c
#    )

