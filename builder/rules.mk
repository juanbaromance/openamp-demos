CA53=a53
CA9=a9
CR5=r5

OBJD  =./build.${target}
SRCD ?=./src
INCD ?=./inc

RC =\033[0m\033[05;31m\033[01;31m\033[04;31m
BC =\033[00;34m
GC  =\033[00;32m
EOC =\033[0m
AUDITOR=PleaseShowMe

BAREMETAL_PLATFORM="bare-metal"
GNULINUX_PLATFORM="gnu-linux"
PLATFORM ?= $(BAREMETAL_PLATFORM)

ifeq ($(target), $(CA53))

 CROSS_PREFIX=aarch64-linux-gnu
 CFLAGS = -c -O3 -DARMA53
 LDLIBS =
 LDLIBS += -lpthread
 PLATFORM = $(GNULINUX_PLATFORM)

else

ifeq ($(target), $(CR5))

CROSS_PREFIX=armr5-none-eabi

CFLAGS = -c -O3 -DARMR5 -mcpu=cortex-r5 -mfloat-abi=hard  -mfpu=vfpv3-d16
CPPFLAGS = $(CFLAGS)

LDFLAGS  = -mcpu=cortex-r5 -mfloat-abi=hard -mfpu=vfpv3-d16
LDFLAGS += -Wl,-T -Wl,./src/lscript.ld -L./dependencies/lib/bsp -L./dependencies/lib/bsp/usr

LDLIBS  = -Wl,--start-group,-lxil,-lgcc,-lc,--end-group
LDLIBS += -Wl,--start-group,-lxil,-lmetal,-lgcc,-lc,--end-group
LDLIBS += -Wl,--start-group -lgcc -lc -lm -lrdimon -lstdc++ -Wl,--end-group
EMULATOR_SPEC = ./r5.qemu.startup.sh

else

ifeq ($(target), $(CA9))

CROSS_PREFIX=arm-none-eabi
MACHINE = -mcpu=cortex-a9 -mfloat-abi=hard  -mfpu=vfpv3
CFLAGS = -c -O3 $(MACHINE)
CPPFLAGS = $(CFLAGS)

LDFLAGS  = $(MACHINE)
LDFLAGS += -Wl,-build-id=none
#LDFLAGS += --specs=rdimon.specs -Wl,--start-group -lgcc -lc -lm -lrdimon -Wl,--end-group
LDFLAGS += --specs=./src/Xilinx.spec
LDFLAGS += -Wl,-T -Wl,./src/lscript.ld
LDFLAGS += -L./dependencies/lib/bsp -L./dependencies/lib/bsp/usr

LDLIBS   = -Wl,--start-group,-lxil,-lfreertos,-lgcc,-lc,--end-group
LDLIBS  += -Wl,--start-group,-lxil,-lmetal,-lopen_amp,-lgcc,-lc,--end-group
LDLIBS  += -Wl,--start-group,-lxil,-lmetal,-lgcc,-lc,--end-group
LDLIBS  += -Wl,--start-group,-lxil,-lgcc,-lc,-lstdc++,--end-group
EMULATOR_SPEC = ./a91.qemu.startup.sh

endif
endif
endif

CXX = $(CROSS_PREFIX)-g++
CPP = $(CROSS_PREFIX)-g++
ELFSIZE   = $(CROSS_PREFIX)-size
ELFREADER = $(CROSS_PREFIX)-readelf

CPPFLAGS += --std=c++17 -Wno-write-strings -O3
CFLAGS += -I./dependencies/include/bsp -I$(SRCD) -I$(INCD) -I./dependencies/include

CPP_OBJECTS = $(addsuffix .o, $(CPP_OBJS) )
C_OBJECTS   = $(addsuffix .o, $(C_OBJS) )
APP_OBJS    = $(CPP_OBJECTS) $(C_OBJECTS)

INSTALL_ARTEFACT = $(basename $(APP) ).$(shell git rev-parse --verify --short HEAD ).elf
LDFLAGS += $(addprefix -L./dependencies/lib/,$(DEPENDENCIES))

vpath %.o ${OBJD}
vpath %.c ${SRCD}
vpath %.cpp ${SRCD}
vpath %.h  ${SRCD} ${INCD} 

.PHONY: all clean
all: $(AUDITOR) $(OBJD) $(APP)

$(APP): $(APP_OBJS)  
	@printf "link : $(GC)$(notdir $^)$(EOC) as $(GC)$@$(EOC)\n"
ifeq ($(VERBOSE), 1)
	@printf "$(GC)Libs/paths$(EOC) : $(LDFLAGS)\n\n"
endif
	$(CPP) $(LDFLAGS) -o $@ $(addprefix ${OBJD}/,$(APP_OBJS)) $(LDLIBS)
ifeq ($(PLATFORM), $(BAREMETAL_PLATFORM))
	@export LANG="";$(ELFSIZE) $@  | tee $@.$(shell git rev-parse --verify --short HEAD ).size
	@export LANG="";$(ELFREADER) -h $@ | grep Entry
endif

$(C_OBJECTS): %.o : %.c
	@printf "c.compile : $(BC)$@$(EOC)\n"
	@$(CPP) $(CFLAGS) -o ${OBJD}/$@ $<

$(CPP_OBJECTS): %.o : %.cpp
	@printf "c++.compile : $(BC)$@$(EOC)\n"
ifeq ($(VERBOSE), 1)
	@printf "$(GC)cpp.flags$(EOC) : $(CPPFLAGS)\n\n"
endif
	@$(CXX) $(CPPFLAGS) -o ${OBJD}/$@ $<

$(OBJD):
	@[ ! -d "${OBJD}" ] && mkdir -p $(OBJD);

$(AUDITOR):
	@printf "\n`date`\n$(BC)$(APP)$(EOC) deployment on $(GC)$(shell $(CPP) -dumpmachine).$(target):${PLATFORM}$(EOC)\n\n"

install: $(APP) $(INSTALL_ARTEFACT)
	@firmware.installer.sh ${INSTALL_ARTEFACT}

$(INSTALL_ARTEFACT): $(APP)
	 @cp $< $@


run: $(APP)
	$(EMULATOR_SPEC) $<

clean:
	@rm -rf $(APP) $(OBJD)/*.o
