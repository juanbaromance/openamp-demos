/* Xilinx includes. */
#include "bsp/xil_printf.h"
#include "modules/module.h"
#include "bspconfig.h"

#ifdef FREERTOS_BSP
#include "bsp/FreeRTOS.h"
#include "bsp/task.h"
#else
#include "modules/mockup.h"
#endif


static void processing( void *task_arg )
{
    size_t *payload = (size_t *)task_arg;
    xil_printf("%s:%p : $%08x\n", __PRETTY_FUNCTION__ , task_arg, *payload );
    vTaskDelete( nullptr );
}

int main( void )
{
    xil_printf( "%s\n", __PRETTY_FUNCTION__ );
    myclass();

    static TaskHandle_t comm_task;
    static size_t task_arg = 0xdeadbeef;
    BaseType_t stat = xTaskCreate( processing, ( const char * ) "HW2", 1024, & task_arg, 2, & comm_task );

    xil_printf( "%s: μkernel scheduled ( %d vs %d )\n", __PRETTY_FUNCTION__ , stat, pdPASS );
    vTaskStartScheduler();
    xil_printf( "%s: μkernel breaks\n", __PRETTY_FUNCTION__ );
    for( ;; );

}

