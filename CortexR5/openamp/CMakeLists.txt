set ( Component openamp-testing )
cmake_minimum_required(VERSION 3.13)


set ( CMAKE_TOOLCHAIN_FILE zynqmp_r5_generic.cmake )
set ( openamp_wrapper OpenAMP.bmr5 )
set ( util Util.bmr5 )

set ( CMAKE_CXX_STANDARD 17 )

set ( Target_deps
    " -L${CMAKE_CURRENT_SOURCE_DIR}/dependencies/lib/bsp/lib"
    " -L${CMAKE_CURRENT_SOURCE_DIR}/dependencies/lib/util "
    " -L${CMAKE_CURRENT_SOURCE_DIR}/dependencies/lib/openamp_wrapper "
    " -L${CMAKE_CURRENT_SOURCE_DIR}/dependencies/lib/bsp/usr/local/lib "
    )

include_directories ( ${CMAKE_CURRENT_SOURCE_DIR}
    ./dependencies/include/bsp
    ./dependencies/include/openamp_wrapper
    ./dependencies/include/util
    ./dependencies/include/modules
    )

add_subdirectory ( modules )
FILE ( GLOB sources $(CMAKE_CURRENT_SOURCE_DIR) *.cpp modules/*.cpp )
# FILE ( GLOB sources $(CMAKE_CURRENT_SOURCE_DIR) main.cpp )

project( ${Component} )
set( Target ${Component}.x.elf )
add_executable( ${Target} ${sources} )

set ( LinkerGroups
    "-Wl,--start-group -lgcc -lc -lm -lrdimon -lstdc++ -Wl,--end-group"
    "-Wl,--start-group -l${openamp_wrapper} -lxil -lmetal -lopen_amp -lgcc -lc -Wl,--end-group"
    "-Wl,--start-group -lgcc -lc -lm -lstdc++ -l${util} -Wl,--end-group"
#    "-Wl,--start-group -lxil -lfreertos -lgcc -lc -Wl,--end-group"
    )
target_link_libraries( ${Target} ${LinkerGroups} )

