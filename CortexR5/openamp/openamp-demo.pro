CONFIG += c++17
INCLUDEPATH  = ./dependencies/include ./dependencies/include/util
INCLUDEPATH += ./dependencies/include/bsp ./dependencies/include/openamp_wrapper
INCLUDEPATH += ./dependencies/include/kernel/include/ ./dependencies/include/kernel/drivers/remoteproc

HEADERS += \
	module.h \
	modules/mockup.h \
	modules/module.h \
	openamp_testing.h

SOURCES += \
	linux/kernel/remoteproc_core.c \
	linux/kernel/remoteproc_elf_loader.c \
	linux/kernel/remoteproc_virtio.c \
	linux/kernel/virtio_rpmsg_bus.c \
	linux/kernel/zynq_remoteproc.c \
	linux/kernel/zynqmp_r5_remoteproc.c \
	linux/meta-openamp/recipes-openamp/rpmsg-examples/rpmsg-mat-mul/mat_mul_demo.c \
	linux/meta-openamp/recipes-openamp/rpmsg-examples/rpmsg-proxy-app/proxy_app.c \
	main.cpp \
	module.cpp \
	modules/mockup.cpp \
	modules/module.cpp \
	openamp_testing.cpp

DISTFILES += \
	CMakeLists.txt \
	firmware.installer.sh \
	freertos/CMakeLists.txt \
	linux/system.dts \
	lscript.ld \
	tools/ZynQopenAMP.sh \
	zynqmp_r5_generic.cmake
