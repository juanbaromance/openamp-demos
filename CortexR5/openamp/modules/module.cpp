#include <iostream>
#include <sstream>
#include <cassert>
#include "bsp/xil_printf.h"
#include "util/Segment7Profiler.h"
#include <cmath>
#include <climits>
#include "openamp_testing.h"
#include "bspconfig.h"

#ifdef FREERTOS_BSP

#include "bsp/FreeRTOS.h"
#include "bsp/timers.h"
#include "bsp/task.h"

#else

#include "mockup.h"

#endif


template <typename T=int>
class MyClass {

    using Tclass = MyClass<T>;

public:
    MyClass( T arg ) : value(arg)
    {
        xTaskCreate( wdgTrampoline, ( const char* ) "Watchdog", configMINIMAL_STACK_SIZE, this, tskIDLE_PRIORITY, & task_handle );
    }
    auto valueOf(){  return value; }
    void dump()
    {
        auto dump = []( auto val, const char* backtrace )
        {
            std::stringstream oss;
            oss << backtrace << ":" << val << "\n";
            xil_printf( "%s\r\n", oss.str().c_str() );
        };
        dump( valueOf(), __PRETTY_FUNCTION__ );
    }
    TaskHandle_t valueOfTask(){ return task_handle; }

private:
    void wdgCallback()
    {
        constexpr const char *backtrace = __PRETTY_FUNCTION__ ;
        uint32_t notification;
        for( ;; )
        {
            xil_printf( "%s : wait(%d) on notification\n", backtrace, deadline );
            if( xTaskNotifyWait( 0x00, ULONG_MAX, & notification, deadline ) == pdFALSE )
            {
                xil_printf( "%s : expired\n", backtrace, notification );
                continue;
            }
            xil_printf( "%s : notified on %08x\n", backtrace, std::bitset<8>(notification).to_ulong() );
            break;
        }
        vTaskDelete( task_handle );
    }

    static void wdgTrampoline( void *args ){ reinterpret_cast<Tclass*>(args)->wdgCallback(); }
    T value;
    TaskHandle_t task_handle;
    constexpr static uint32_t deadline{pdMS_TO_TICKS(6000)};
};



template <typename T = void>
class cTimer {
public:
    using callback_t = void(*)(T);
    enum class Mode {
        SingleShot = pdFALSE,
        Cyclic = ! SingleShot,
    };

    enum class Action {
        Go = true,
        Stop = ! Go,
        AfterWards = Stop
    };

    cTimer( const char* name_ , callback_t callback = nullptr, Mode mode = Mode::SingleShot, const TickType_t deadline_ = pdMS_TO_TICKS( 10000UL ),
           Action action = Action::Go ) :
          name( name_ ), deadline( deadline_ )

    {
        const TickType_t elapsed = pdMS_TO_TICKS( deadline );
        xTimer = xTimerCreate( name, elapsed, enumCollapse(mode), (void *)this, trampoline );
        configASSERT( xTimer );
        user_callback = callback;
        if( action == Action::Go )
        {
            std::stringstream oss;
            oss << name << ".Threaded( " << deadline << " )( " << ( start = xTaskGetTickCount() ) << ")";
            dump( oss.str().c_str() );
            xTimerStart( xTimer, 0 );
        }
    }
    ~cTimer(){ xTimerStop( xTimer, 0 ); }

    cTimer& watchdog( TaskHandle_t task_handler ){ xWatchdog = task_handler; return *this; }
    void action( Action activity = Action::Go )
    {
        if ( activity == Action::Go )
            xTimerStart( xTimer, 0 );
        else
            xTimerStop( xTimer, 0 );
    }

    void dump( const char* backtrace )
    {
        std::stringstream oss;
        oss << __PRETTY_FUNCTION__ << ":" << backtrace << "\n";
        xil_printf( "%s\n", oss.str().c_str() );
    }

    void reset(){ if( xWatchdog ) xTaskNotify( valueOfWatchdog(), 1, eSetBits ); }
    long start;

private:
    TaskHandle_t valueOfWatchdog(){ return xWatchdog; }
    static void trampoline( TimerHandle_t pxTimer )
    {
        configASSERT( pxTimer );
        cTimer *t = reinterpret_cast<cTimer*>( pvTimerGetTimerID( pxTimer ));
        if( t->user_callback )
            t->user_callback(t);
    }

    TickType_t deadline;
    callback_t user_callback;
    TimerHandle_t xTimer;
    TaskHandle_t xWatchdog{nullptr};
    const char* name;
};


#include <memory>
using MyTimer = cTimer<void*>;
static std::unique_ptr<MyTimer> timer;
static std::unique_ptr<MyClass<>> myClass;

extern "C"{

void myclass()
{
    ( myClass = std::make_unique<MyClass<>>(-100) )->dump();

    S7Profiler s;
    s.setAccLimit(300);
    s.setVelLimit(150);
    auto tmp = []( const struct S7Profiler::point & point )
    {
        return;
        std::stringstream oss;
        oss << __PRETTY_FUNCTION__ <<  " TPVA(" << point.t << "," << point.p << "," << point.v << "," << point.a << ")";
        xil_printf( "%s\n", oss.str().c_str() );
    };

    auto callback = []( void *t )
    {
        MyTimer & timer = * reinterpret_cast< MyTimer* >(t);
        auto elapsed = xTaskGetTickCount();
        timer.dump( ( std::string( __PRETTY_FUNCTION__ ) + " :triggered after " + std::to_string(elapsed) ).c_str() );
        timer.reset();

    };

    timer = std::make_unique<MyTimer>( __PRETTY_FUNCTION__ , callback, MyTimer::Mode::SingleShot, 5000, MyTimer::Action::AfterWards );
    timer->watchdog( myClass->valueOfTask() ).action(  MyTimer::Action::Go );
    s.generator( 12*M_PI, 1.8, tmp );

    openAMPTest();
}

}
