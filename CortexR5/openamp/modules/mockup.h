#pragma once
#include <stddef.h>

using TaskHandle_t = void*;
enum BaseType_t
{
    pdPASS = true,
    pdFALSE = ! pdPASS
};

enum {
    configMINIMAL_STACK_SIZE = 256
};

enum {
    tskIDLE_PRIORITY  = 0
};

void vTaskDelete( TaskHandle_t handle = nullptr );
void vTaskStartScheduler();
using callback = void (*)(void *arg);
BaseType_t xTaskCreate( callback cb, const char *name, size_t stack_size, void *user_arg, int priority, TaskHandle_t *handle );

#include <cstdint>
#define pdMS_TO_TICKS(a) (a/500)

using TickType_t = uint32_t;
using TimerHandle_t = void*;
enum eOperation{
    eSetBits
};
BaseType_t xTaskNotifyWait(int unknown, size_t max, uint32_t *notification, uint32_t deadline );

using timer_callback = void (*)( TimerHandle_t );
TimerHandle_t xTimerCreate( const char *name, const TickType_t elapsed, int activity,  void *user_arg, timer_callback trampoline );
void xTimerStart(TimerHandle_t,uint32_t delay);
void xTimerStop(TimerHandle_t,uint32_t delay);
long xTaskGetTickCount();
void configASSERT( TimerHandle_t );
void* pvTimerGetTimerID(TimerHandle_t);
void xTaskNotify(TaskHandle_t,size_t trigger, enum eOperation operation );
