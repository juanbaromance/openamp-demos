var searchData=
[
  ['cache_20interfaces',['CACHE Interfaces',['../group__cache.html',1,'']]],
  ['cache_2eh',['cache.h',['../cache_8h.html',1,'(Global Namespace)'],['../system_2freertos_2cache_8h.html',1,'(Global Namespace)'],['../system_2generic_2cache_8h.html',1,'(Global Namespace)'],['../system_2linux_2cache_8h.html',1,'(Global Namespace)'],['../system_2nuttx_2cache_8h.html',1,'(Global Namespace)']]],
  ['close',['close',['../structmetal__io__ops.html#a25310b7272817558438e1feb03b24d14',1,'metal_io_ops']]],
  ['cls_5fname',['cls_name',['../structlinux__driver.html#acc2b5d168d0faf4c35c3448fa4dfe09e',1,'linux_driver']]],
  ['cls_5fpath',['cls_path',['../structlinux__device.html#aa90d44d9691dc7aa83164445cccbc30a',1,'linux_device']]],
  ['cnt',['cnt',['../struction__heap__query.html#ac74dbc87052eaf984aa8bc4eb8cd8107',1,'ion_heap_query']]],
  ['common',['common',['../structmetal__state.html#aa36ac30ed6ef6439baea425191af0968',1,'metal_state']]],
  ['compiler_2eh',['compiler.h',['../compiler_2gcc_2compiler_8h.html',1,'(Global Namespace)'],['../compiler_2iar_2compiler_8h.html',1,'(Global Namespace)'],['../compiler_8h.html',1,'(Global Namespace)']]],
  ['cond',['cond',['../structmetal__condition.html#aeb36c11bc675cf940b9d027ccc7a214b',1,'metal_condition']]],
  ['condition_20variable_20interfaces',['Condition Variable Interfaces',['../group__condition.html',1,'']]],
  ['condition_2ec',['condition.c',['../freertos_2condition_8c.html',1,'(Global Namespace)'],['../generic_2condition_8c.html',1,'(Global Namespace)'],['../linux_2condition_8c.html',1,'(Global Namespace)'],['../nuttx_2condition_8c.html',1,'(Global Namespace)']]],
  ['condition_2eh',['condition.h',['../condition_8h.html',1,'(Global Namespace)'],['../system_2freertos_2condition_8h.html',1,'(Global Namespace)'],['../system_2generic_2condition_8h.html',1,'(Global Namespace)'],['../system_2linux_2condition_8h.html',1,'(Global Namespace)'],['../system_2nuttx_2condition_8h.html',1,'(Global Namespace)']]],
  ['config_2eh',['config.h',['../config_8h.html',1,'']]],
  ['cpu_2eh',['cpu.h',['../cpu_8h.html',1,'(Global Namespace)'],['../processor_2aarch64_2cpu_8h.html',1,'(Global Namespace)'],['../processor_2arm_2cpu_8h.html',1,'(Global Namespace)'],['../processor_2ceva_2cpu_8h.html',1,'(Global Namespace)'],['../processor_2csky_2cpu_8h.html',1,'(Global Namespace)'],['../processor_2microblaze_2cpu_8h.html',1,'(Global Namespace)'],['../processor_2riscv_2cpu_8h.html',1,'(Global Namespace)'],['../processor_2x86__64_2cpu_8h.html',1,'(Global Namespace)']]]
];
