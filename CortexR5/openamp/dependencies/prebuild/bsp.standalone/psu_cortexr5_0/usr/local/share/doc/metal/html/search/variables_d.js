var searchData=
[
  ['page_5fmask',['page_mask',['../structmetal__io__region.html#ae10d7c45fa9e87d93dbb01e87cd7f822',1,'metal_io_region']]],
  ['page_5fshift',['page_shift',['../structmetal__io__region.html#a78412204cb2dd9ebed613b3273b9ab1b',1,'metal_io_region::page_shift()'],['../structmetal__page__size.html#a990ff70e16c6efaaf58e47b0948d23a7',1,'metal_page_size::page_shift()'],['../structmetal__state.html#ac47375715b2e69ecb17b3a3714ae5c42',1,'metal_state::page_shift()']]],
  ['page_5fsize',['page_size',['../structmetal__page__size.html#a97c5f203beaedb46b0d52425e5445a08',1,'metal_page_size::page_size()'],['../structmetal__state.html#a3ce0623d9131ad2f99e47e78ec3feba3',1,'metal_state::page_size()']]],
  ['page_5fsizes',['page_sizes',['../structmetal__state.html#a4488b4117a68b92e40c0c04f53f7310b',1,'metal_state']]],
  ['pagemap_5ffd',['pagemap_fd',['../structmetal__state.html#a197661bddfd13e2af962db5802989261',1,'metal_state']]],
  ['path',['path',['../structmetal__page__size.html#aea1125ee871f2ddaca6580fc78985113',1,'metal_page_size']]],
  ['phys',['phys',['../structlinux__device__shm__io__region.html#af45d80e9eb42b1eb4c18f64a73734486',1,'linux_device_shm_io_region']]],
  ['phys_5fto_5foffset',['phys_to_offset',['../structmetal__io__ops.html#a4f7597eddef002c9a66eb042179c016c',1,'metal_io_ops']]],
  ['physmap',['physmap',['../structmetal__io__region.html#a129e040d18e8e6570ed5ce2b7c258e86',1,'metal_io_region']]],
  ['prev',['prev',['../structmetal__list.html#ada1c8bdeec046c44a68e5adb22813409',1,'metal_list']]],
  ['priv',['priv',['../group__shmem-provider.html#ga9e9b6509428182a91d1025c288a9d32e',1,'metal_shm_provider']]],
  ['provider',['provider',['../structmetal__generic__shmem.html#a4129b3668afd1323d2b7c70179af0eb0',1,'metal_generic_shmem::provider()'],['../structmetal__linux__shm__provider__ion.html#a1dc7c8cb1697e891a48f795da7e34645',1,'metal_linux_shm_provider_ion::provider()']]]
];
