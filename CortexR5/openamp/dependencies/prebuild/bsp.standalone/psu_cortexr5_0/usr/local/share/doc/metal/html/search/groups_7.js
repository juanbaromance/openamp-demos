var searchData=
[
  ['scatter_20list_20interfaces',['Scatter List Interfaces',['../group__scatterlist.html',1,'']]],
  ['shared_20memory_20interfaces',['Shared Memory Interfaces',['../group__shmem.html',1,'']]],
  ['shared_20memory_20provider_20interfaces',['Shared Memory Provider Interfaces',['../group__shmem-provider.html',1,'']]],
  ['sleep_20interfaces',['Sleep Interfaces',['../group__sleep.html',1,'']]],
  ['spinlock_20interfaces',['Spinlock Interfaces',['../group__spinlock.html',1,'']]],
  ['simple_20utilities',['Simple Utilities',['../group__utilities.html',1,'']]]
];
