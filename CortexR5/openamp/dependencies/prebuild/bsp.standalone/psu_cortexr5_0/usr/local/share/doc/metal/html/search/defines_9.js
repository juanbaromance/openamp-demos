var searchData=
[
  ['uio_5fdmabuf_5fdir_5fbidir',['UIO_DMABUF_DIR_BIDIR',['../uio_8h.html#a27daced4da8e1df4640ba7cf9fe600a2',1,'uio.h']]],
  ['uio_5fdmabuf_5fdir_5ffrom_5fdev',['UIO_DMABUF_DIR_FROM_DEV',['../uio_8h.html#aa7fab52d11d4908578c0599fc1f2975b',1,'uio.h']]],
  ['uio_5fdmabuf_5fdir_5fnone',['UIO_DMABUF_DIR_NONE',['../uio_8h.html#a9f73993669186397c7ed5f5c818b75a0',1,'uio.h']]],
  ['uio_5fdmabuf_5fdir_5fto_5fdev',['UIO_DMABUF_DIR_TO_DEV',['../uio_8h.html#a926cba36341afb28fba722f1f5ab53f2',1,'uio.h']]],
  ['uio_5fioc_5fbase',['UIO_IOC_BASE',['../uio_8h.html#a9be73b9fb71407b57f4443cfe02f2b11',1,'uio.h']]],
  ['uio_5fioc_5fmap_5fdmabuf',['UIO_IOC_MAP_DMABUF',['../uio_8h.html#a7da3d8035cdee66bd7209f42f0f0caf5',1,'uio.h']]],
  ['uio_5fioc_5funmap_5fdmabuf',['UIO_IOC_UNMAP_DMABUF',['../uio_8h.html#a758b1d9d367d098ec84d2104b62cd53c',1,'uio.h']]]
];
