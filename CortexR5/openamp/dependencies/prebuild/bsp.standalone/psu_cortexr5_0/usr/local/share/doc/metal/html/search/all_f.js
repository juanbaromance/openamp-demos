var searchData=
[
  ['read',['read',['../structmetal__io__ops.html#a3cba7140674cb2eeb9fc74ad614ea9fb',1,'metal_io_ops']]],
  ['readme_2emd',['README.md',['../_r_e_a_d_m_e_8md.html',1,'']]],
  ['refcount',['refcount',['../structmetal__generic__shmem.html#a9e6affae8354b751553495b2923d81ed',1,'metal_generic_shmem']]],
  ['refs',['refs',['../structmetal__generic__shmem.html#a5e0e43898bd9b7c7b97a698fa5c1dd3b',1,'metal_generic_shmem']]],
  ['region_5fphys',['region_phys',['../structlinux__device.html#a2f8e2d069a9703f4e7a7f3073e8d9df2',1,'linux_device']]],
  ['regions',['regions',['../structmetal__device.html#ad6a86618d3c23da03de5d2b0961a8ba1',1,'metal_device']]],
  ['reserved0',['reserved0',['../struction__heap__data.html#a7247e63270bfb7c6c7e5bbe5a430fbe7',1,'ion_heap_data::reserved0()'],['../struction__heap__query.html#aaa117326542bff173bbcdba349398355',1,'ion_heap_query::reserved0()']]],
  ['reserved1',['reserved1',['../struction__heap__data.html#a6713b56c7f0876c2a285587074176172',1,'ion_heap_data::reserved1()'],['../struction__heap__query.html#ab9286cd698857f63642c5ba11e90370c',1,'ion_heap_query::reserved1()']]],
  ['reserved2',['reserved2',['../struction__heap__data.html#a3d5f86835518e14d06399921553f2d8a',1,'ion_heap_data::reserved2()'],['../struction__heap__query.html#ad040773a097a165e7cd49db2431e4610',1,'ion_heap_query::reserved2()']]],
  ['restrict',['restrict',['../compiler_2gcc_2compiler_8h.html#a080abdcb9c02438f1cd2bb707af25af8',1,'restrict():&#160;compiler.h'],['../compiler_2iar_2compiler_8h.html#a080abdcb9c02438f1cd2bb707af25af8',1,'restrict():&#160;compiler.h']]]
];
