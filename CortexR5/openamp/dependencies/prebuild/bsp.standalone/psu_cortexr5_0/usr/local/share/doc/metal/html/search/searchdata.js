var indexSectionsWithContent =
{
  0: "_abcdfghilmnopqrstuvw",
  1: "ilmu",
  2: "acdilmrstuv",
  3: "_mst",
  4: "_abcdfghilmnopqrstuvw",
  5: "am",
  6: "im",
  7: "im",
  8: "_afghimnru",
  9: "abcdilmst",
  10: "ls"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Modules",
  10: "Pages"
};

