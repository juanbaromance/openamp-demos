var searchData=
[
  ['ion_5fheap_5ftype_5fcarveout',['ION_HEAP_TYPE_CARVEOUT',['../ion_8h.html#af4b4f1b113d4bd4095b7a75f1bd52076a6d1adf95d68ece0d8b46bf9d766f660c',1,'ion.h']]],
  ['ion_5fheap_5ftype_5fchunk',['ION_HEAP_TYPE_CHUNK',['../ion_8h.html#af4b4f1b113d4bd4095b7a75f1bd52076a779b88a5a68c5aeee7aefccf6bfeb376',1,'ion.h']]],
  ['ion_5fheap_5ftype_5fcustom',['ION_HEAP_TYPE_CUSTOM',['../ion_8h.html#af4b4f1b113d4bd4095b7a75f1bd52076ac470795f8cf9ec655e99f163851276b2',1,'ion.h']]],
  ['ion_5fheap_5ftype_5fdma',['ION_HEAP_TYPE_DMA',['../ion_8h.html#af4b4f1b113d4bd4095b7a75f1bd52076a9800bb3987bbbcf59ca21ede17e25b86',1,'ion.h']]],
  ['ion_5fheap_5ftype_5fsystem',['ION_HEAP_TYPE_SYSTEM',['../ion_8h.html#af4b4f1b113d4bd4095b7a75f1bd52076adc125abcfbf69dbb0f0651d298d4190d',1,'ion.h']]],
  ['ion_5fheap_5ftype_5fsystem_5fcontig',['ION_HEAP_TYPE_SYSTEM_CONTIG',['../ion_8h.html#af4b4f1b113d4bd4095b7a75f1bd52076a679ebda57f49bd280634d51edc5e9881',1,'ion.h']]]
];
