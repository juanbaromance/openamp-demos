var searchData=
[
  ['metal_5fcntr_5firq_5fregister',['metal_cntr_irq_register',['../group__irq.html#ga22c994407d005793e478ac8267744f88',1,'irq_controller.h']]],
  ['metal_5firq_5fhandler',['metal_irq_handler',['../group__irq.html#ga4fd6caa764267fc62373ae22e17aea2a',1,'irq.h']]],
  ['metal_5firq_5fset_5fenable',['metal_irq_set_enable',['../group__irq.html#ga125a768ecd5924b3da91b6ebb6820e9a',1,'irq_controller.h']]],
  ['metal_5firq_5ft',['metal_irq_t',['../group__system.html#ga5b3ecd7ba1914b26547392ceadec813c',1,'sys.h']]],
  ['metal_5flog_5fhandler',['metal_log_handler',['../group__logging.html#gaa79b2a8962227bb67c242b98afda292a',1,'log.h']]],
  ['metal_5fmutex_5ft',['metal_mutex_t',['../system_2nuttx_2mutex_8h.html#aad22123e8beb290ce87ded38c245166d',1,'mutex.h']]],
  ['metal_5fphys_5faddr_5ft',['metal_phys_addr_t',['../group__system.html#gae024fa10b72199a3e26c29b6eb97df5d',1,'sys.h']]],
  ['metal_5fshm_5falloc',['metal_shm_alloc',['../group__shmem-provider.html#ga9a2023a6d99d8db9c482b110260bcff7',1,'shmem-provider.h']]],
  ['metal_5fshm_5ffree',['metal_shm_free',['../group__shmem-provider.html#ga16952615e53046b65dd9b7456bff13d6',1,'shmem-provider.h']]]
];
