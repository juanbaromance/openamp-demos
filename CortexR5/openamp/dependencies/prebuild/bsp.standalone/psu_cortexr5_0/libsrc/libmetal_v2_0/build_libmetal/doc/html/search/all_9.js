var searchData=
[
  ['libmetal',['libmetal',['../index.html',1,'']]],
  ['ldrv',['ldrv',['../structlinux__device.html#ae2c25385fa957c66ac9b1fbd9cba95a2',1,'linux_device']]],
  ['len',['len',['../structmetal__sg.html#a508be17cd652243a3b8b369a0b23b065',1,'metal_sg::len()'],['../struction__allocation__data.html#af148427efe423fccd4812dd59da48531',1,'ion_allocation_data::len()']]],
  ['license_2emd',['LICENSE.md',['../_l_i_c_e_n_s_e_8md.html',1,'']]],
  ['linux_5fbus',['linux_bus',['../structlinux__bus.html',1,'linux_bus'],['../system_2linux_2device_8c.html#aee50f0231ab78c8aaa2d2c6826a85fab',1,'linux_bus():&#160;device.c']]],
  ['linux_5fdevice',['linux_device',['../structlinux__device.html',1,'']]],
  ['linux_5fdevice_5fshm_5fio_5fregion',['linux_device_shm_io_region',['../structlinux__device__shm__io__region.html',1,'']]],
  ['linux_5fdriver',['linux_driver',['../structlinux__driver.html',1,'']]],
  ['linux_5fion',['linux_ion',['../shmem-provider-ion_8c.html#a6d3c8d370074743d41a48a22fb24211c',1,'shmem-provider-ion.c']]],
  ['linux_5fshm_5fprovider_5fshm',['linux_shm_provider_shm',['../group__shmem.html#ga24afca174985f511dd7254286300af58',1,'shmem.h']]],
  ['list_20primitives',['List Primitives',['../group__list.html',1,'']]],
  ['list_2eh',['list.h',['../list_8h.html',1,'']]],
  ['lock',['lock',['../structmetal__generic__shmem.html#a40328ffa2b85204c66483d11d9bc1bae',1,'metal_generic_shmem']]],
  ['log_2ec',['log.c',['../log_8c.html',1,'']]],
  ['log_2eh',['log.h',['../log_8h.html',1,'(Global Namespace)'],['../system_2freertos_2log_8h.html',1,'(Global Namespace)'],['../system_2generic_2log_8h.html',1,'(Global Namespace)'],['../system_2linux_2log_8h.html',1,'(Global Namespace)'],['../system_2nuttx_2log_8h.html',1,'(Global Namespace)']]],
  ['log_5fhandler',['log_handler',['../structmetal__init__params.html#a003ca9040adcaada7f03faaef46f1b59',1,'metal_init_params::log_handler()'],['../structmetal__common__state.html#abc8928c9100135369eeba2900ba860ea',1,'metal_common_state::log_handler()']]],
  ['log_5flevel',['log_level',['../structmetal__init__params.html#aafc486469d934fd02ea4926ba1d377cc',1,'metal_init_params::log_level()'],['../structmetal__common__state.html#a326c2fb27f8d538f7501d02bd6c3a851',1,'metal_common_state::log_level()']]],
  ['library_20logging_20interfaces',['Library Logging Interfaces',['../group__logging.html',1,'']]],
  ['library_20version_20interfaces',['Library Version Interfaces',['../group__versions.html',1,'']]]
];
