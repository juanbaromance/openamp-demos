var searchData=
[
  ['have_5ffutex_5fh',['HAVE_FUTEX_H',['../config_8h.html#a0be868b22ea37ce37ddfe7595d1d08d4',1,'config.h']]],
  ['have_5fstdatomic_5fh',['HAVE_STDATOMIC_H',['../config_8h.html#a0be0d91730b91396b1236bc6080b014c',1,'config.h']]],
  ['hd',['hd',['../structmetal__irq.html#a8969714de2c7866c68fd8d9bee088618',1,'metal_irq']]],
  ['heap_5fid',['heap_id',['../struction__heap__data.html#afcae072dcc7e932e6c27dd483001a70f',1,'ion_heap_data::heap_id()'],['../structmetal__linux__shm__provider__ion.html#a9ccc532fa29f71fa5ded872b98008ab4',1,'metal_linux_shm_provider_ion::heap_id()']]],
  ['heap_5fid_5fmask',['heap_id_mask',['../struction__allocation__data.html#a099627af88d3992e679b5fc5fbc52740',1,'ion_allocation_data']]],
  ['heaps',['heaps',['../struction__heap__query.html#a63d30f5705fc90e76c3441dfd0100052',1,'ion_heap_query']]]
];
