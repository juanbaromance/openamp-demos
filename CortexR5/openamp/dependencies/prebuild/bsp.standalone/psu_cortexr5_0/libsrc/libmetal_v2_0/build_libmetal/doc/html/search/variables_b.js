var searchData=
[
  ['name',['name',['../structmetal__bus.html#a38fdc374275d87bb04dab29f9ad53e0b',1,'metal_bus::name()'],['../structmetal__device.html#a25e7f2938bdfbb0f44ce11331b93950c',1,'metal_device::name()'],['../group__shmem-provider.html#ga95d189184d32ccf0833763b016d26e86',1,'metal_shm_provider::name()'],['../structmetal__generic__shmem.html#ae64139419fd8b5b3648850c4cabc4d35',1,'metal_generic_shmem::name()'],['../struction__heap__data.html#a7df4fbaed808016ceaf77c057600feb8',1,'ion_heap_data::name()'],['../structmetal__linux__shm__provider__ion.html#a3f8c00ec52f7552a965df2292c5295e0',1,'metal_linux_shm_provider_ion::name()']]],
  ['nents',['nents',['../structmetal__scatter__list.html#a1d8491b7ee084dc0be7d82e4e3777d61',1,'metal_scatter_list']]],
  ['next',['next',['../structmetal__list.html#ac230ff75196e672a80c4a4520294a7fc',1,'metal_list']]],
  ['node',['node',['../structmetal__bus.html#a1af389eaabcb3daa33e7794acfbe446b',1,'metal_bus::node()'],['../structmetal__device.html#acafd9802f3390dfefbdb6040be7028db',1,'metal_device::node()'],['../structmetal__irq__controller.html#a26e87c0da6978319075db2289b901749',1,'metal_irq_controller::node()'],['../group__shmem-provider.html#ga2b043fd7bd3f2343061baab0ab7b4123',1,'metal_shm_provider::node()'],['../structmetal__shm__ref.html#a54b91821f3a6cb3df251f12b8c7b9261',1,'metal_shm_ref::node()'],['../structmetal__generic__shmem.html#ad944a8261f79fa83d29c9d7cfd7b2103',1,'metal_generic_shmem::node()']]],
  ['num_5fpage_5fsizes',['num_page_sizes',['../structmetal__state.html#a08ea681743f49b2410a685518b033ce3',1,'metal_state']]],
  ['num_5fregions',['num_regions',['../structmetal__device.html#a461683551e1ae7c691a5c5efd085960e',1,'metal_device']]]
];
