set(CMAKE_HOST_SYSTEM "Linux-5.0.0-38-generic")
set(CMAKE_HOST_SYSTEM_NAME "Linux")
set(CMAKE_HOST_SYSTEM_VERSION "5.0.0-38-generic")
set(CMAKE_HOST_SYSTEM_PROCESSOR "x86_64")

include("/home/juanba/xilinx-development/BareMetal/CortexR5/BMCortexR5.bsp/standalone_openamp/psu_cortexr5_0/libsrc/libmetal_v2_0/src/libmetal/cmake/platforms/toolchain.cmake")

set(CMAKE_SYSTEM "Generic")
set(CMAKE_SYSTEM_NAME "Generic")
set(CMAKE_SYSTEM_VERSION "")
set(CMAKE_SYSTEM_PROCESSOR "arm")

set(CMAKE_CROSSCOMPILING "TRUE")

set(CMAKE_SYSTEM_LOADED 1)
