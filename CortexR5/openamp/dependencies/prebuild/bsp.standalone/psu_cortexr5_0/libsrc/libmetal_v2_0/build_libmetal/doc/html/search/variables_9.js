var searchData=
[
  ['ldrv',['ldrv',['../structlinux__device.html#ae2c25385fa957c66ac9b1fbd9cba95a2',1,'linux_device']]],
  ['len',['len',['../structmetal__sg.html#a508be17cd652243a3b8b369a0b23b065',1,'metal_sg::len()'],['../struction__allocation__data.html#af148427efe423fccd4812dd59da48531',1,'ion_allocation_data::len()']]],
  ['linux_5fbus',['linux_bus',['../system_2linux_2device_8c.html#aee50f0231ab78c8aaa2d2c6826a85fab',1,'device.c']]],
  ['linux_5fion',['linux_ion',['../shmem-provider-ion_8c.html#a6d3c8d370074743d41a48a22fb24211c',1,'shmem-provider-ion.c']]],
  ['linux_5fshm_5fprovider_5fshm',['linux_shm_provider_shm',['../group__shmem.html#ga24afca174985f511dd7254286300af58',1,'shmem.h']]],
  ['lock',['lock',['../structmetal__generic__shmem.html#a40328ffa2b85204c66483d11d9bc1bae',1,'metal_generic_shmem']]],
  ['log_5fhandler',['log_handler',['../structmetal__init__params.html#a003ca9040adcaada7f03faaef46f1b59',1,'metal_init_params::log_handler()'],['../structmetal__common__state.html#abc8928c9100135369eeba2900ba860ea',1,'metal_common_state::log_handler()']]],
  ['log_5flevel',['log_level',['../structmetal__init__params.html#aafc486469d934fd02ea4926ba1d377cc',1,'metal_init_params::log_level()'],['../structmetal__common__state.html#a326c2fb27f8d538f7501d02bd6c3a851',1,'metal_common_state::log_level()']]]
];
