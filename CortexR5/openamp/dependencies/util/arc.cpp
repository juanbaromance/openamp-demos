#include <cmath>
#include <valarray>
#include <iostream>

#include "arc.h"
enum {
    rad_input = true
};

#ifndef M_PI
#define M_PI 3.14159
#endif

template <typename T>
T converter( T val ) { return val* ( M_PI / 180.0 ); }
template <bool rad_mode, typename T>
T converter( T val ) { return val * ( 180.0 / M_PI ); }


iArc::iArc(const size_t aperture_ , const size_t phulcro_ ) :
      aperture( aperture_ ), phulcro( phulcro_ ), density(800), x_offset(0), y_offset(0), angular_offset(0) {}

iArc &iArc::setAperture ( const size_t deg_     ){ aperture = deg_ % 360; return *this; }
iArc &iArc::setFulcrum  ( const size_t phulcro_ ){ phulcro = phulcro_ ; return *this; }
iArc &iArc::Offset(const size_t cord_ , const size_t radial_ , const size_t angular_ )
{
    x_offset = cord_;
    y_offset = radial_ ;
    angular_offset = angular_ ;
    return *this;
}

iArc::profileT iArc::profile( const std::string & backtrace )
{

    using namespace std;
    double left_angle  = converter( 90. + aperture/2);
    double right_angle = converter( 90. - aperture/2 );
    double step( M_PI/density );
    size_t no_iterations = static_cast<size_t>( round( fabs( left_angle - right_angle ) / step ) );
    valarray<double> v(no_iterations), x(no_iterations), y(no_iterations);
    valarray<size_t> iteration(no_iterations);

    for( size_t i = 0; i < no_iterations; i++ )
    {
        iteration[i] = i;
        v[ i ] = left_angle - ( i * step );
        x[ i ] = ( phulcro * cos( v[i] ) ) + x_offset;
        y[ i ] = ( phulcro * sin( v[i] ) ) + y_offset;
        v[ i ] = converter<rad_input>( v[ i ] ) + angular_offset;
    }

    if( backtrace.empty() == false )
        for( auto i : iteration )
            ( cout << i << " " << v[i] << " " << x[i] << " " << y[i] << "\n").flush();
    return current_profile = make_tuple(v,x,y);
}




