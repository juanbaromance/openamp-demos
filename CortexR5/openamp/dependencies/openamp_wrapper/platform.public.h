#pragma once

#if defined __cplusplus
extern "C" {
#endif

/**
 * platform_init - initialize the platform
 * It will initialize the platform.
 * @argc: number of arguments
 * @argv: array of the input arguements
 * @platform: pointer to store the platform data pointer
 * return 0 for success or negative value for failure
 */

int platform_init(int argc, char *argv[], void **platform);

/**
 * platform_create_rpmsg_vdev - create rpmsg vdev
 * It will create rpmsg virtio device, and returns the rpmsg virtio
 * device pointer.
 * @platform: pointer to the private data
 * @vdev_index: index of the virtio device, there can more than one vdev
 *              on the platform.
 * @role: virtio master or virtio slave of the vdev
 * @rst_cb: virtio device reset callback
 * @ns_bind_cb: rpmsg name service bind callback
 * return pointer to the rpmsg virtio device
 */

#include <stdint.h>
typedef struct rpmsg_device rpmsg_device;
typedef struct rpmsg_endpoint rpmsg_endpoint;

struct virtio_device;
typedef void ( *operation_cb )( rpmsg_device *rdev, const char *name, uint32_t dest);
typedef void ( *reset_cb     )( struct virtio_device* );
enum VirtIORole {
    VirtIOMaster = 0,
    VirtiIOSlave = 1,
};
rpmsg_device* platform_create_vdev(void *platform, unsigned int vdev_index, enum VirtIORole role, reset_cb reset_cb, operation_cb operation_cb );

/**
 * platform_release_rpmsg_vdev - release rpmsg virtio device
 * @rpdev: pointer to the rpmsg device
 */
void platform_release_vdev(rpmsg_device *rpdev, rpmsg_endpoint *epoint );

#include <stddef.h>
typedef int  (*epoint_operation )( rpmsg_endpoint *ept, void *data, size_t len, uint32_t src, void *priv);
typedef void (*epoint_reset     )( rpmsg_endpoint *ept );
rpmsg_endpoint* platform_create_ept( rpmsg_device *rdev, const char *ept_name, uint32_t start, uint32_t end, epoint_operation epo, epoint_reset epr );

/**
 * platform_poll - platform poll function
 * @platform: pointer to the platform
 * return negative value for errors, otherwise 0.
 */
int platform_poll(void *arg );

/**
 * platform_cleanup - clean up the platform resource
 * @platform: pointer to the platform
 */
void platform_cleanup(void *platform);

/**
 * platform_xmitt_ept
 * @ept: pointer to endpoint attached to virtio
 * @data: pointer to payload
 * @len: payload length
 * return negative value for errors, otherwise 0.
 */
int platform_xmitt_ept(rpmsg_endpoint *ept, void *data, size_t len );

#if defined __cplusplus
}
#endif
