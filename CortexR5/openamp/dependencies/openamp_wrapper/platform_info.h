#ifndef PLATFORM_INFO_H_
#define PLATFORM_INFO_H_

#include <openamp/remoteproc.h>
#include <openamp/virtio.h>
#include <openamp/rpmsg.h>

#if defined __cplusplus
extern "C" {
#endif

/* Cortex R5 memory attributes */
#define DEVICE_SHARED       0x00000001U /* device, shareable */
#define DEVICE_NONSHARED    0x00000010U /* device, non shareable */
#define NORM_NSHARED_NCACHE 0x00000008U /* Non cacheable  non shareable */
#define NORM_SHARED_NCACHE  0x0000000CU /* Non cacheable shareable */
#define PRIV_RW_USER_RW     (0x00000003U<<8U) /* Full Access */

#if XPAR_CPU_ID == 0
#define SHARED_MEM_PA  0x3ED40000UL
#else
#define SHARED_MEM_PA  0x3EF40000UL
#endif /* XPAR_CPU_ID */
#define SHARED_MEM_SIZE 0x100000UL
#define SHARED_BUF_OFFSET 0x8000UL

enum MemorySpecs {
    OCMWindow_PA = XPAR_PSU_OCM_RAM_0_S_AXI_BASEADDR,
    OCMScope     = 0x20000UL,
    OCMRingTX    = OCMWindow_PA,
    OCMRingRX    = OCMWindow_PA + 0x4000,
    OCMRingSize  = 64,

    R0_DDRWindow_PA = 0x3ED40000UL,
    R1_DDRWindow_PA = 0x3EF40000UL,
    DDRScope        = SHARED_MEM_SIZE,
    DDRRingTX       = FW_RSC_U32_ADDR_ANY,
    DDRRingRX       = FW_RSC_U32_ADDR_ANY,
    DDRRingSize     = 256,
};

/* Interrupt vectors */
#define IPI_IRQ_VECT_ID         XPAR_XIPIPSU_0_INT_ID

struct remoteproc_priv {
	const char *ipi_name; /**< IPI device name */
	const char *ipi_bus_name; /**< IPI bus name */
	struct metal_device *ipi_dev; /**< pointer to IPI device */
	struct metal_io_region *ipi_io; /**< pointer to IPI i/o region */
	unsigned int ipi_chn_mask; /**< IPI channel mask */
	atomic_int ipi_nokick;

};

typedef struct remoteproc remoteproc;
typedef struct remoteproc_priv remoteproc_priv;
#if defined __cplusplus
}
#endif

#endif /* PLATFORM_INFO_H_ */
