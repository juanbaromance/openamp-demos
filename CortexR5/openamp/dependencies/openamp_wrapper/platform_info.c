/*
 * Copyright (c) 2014, Mentor Graphics Corporation
 * All rights reserved.
 * Copyright (c) 2017 Xilinx, Inc.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

/**************************************************************************
 * FILE NAME
 *
 *       platform_info.c
 *
 * DESCRIPTION
 *
 *       This file define platform specific data and implements APIs to set
 *       platform specific information for OpenAMP.
 *
 **************************************************************************/

#include <errno.h>
#include <metal/atomic.h>
#include <metal/assert.h>
#include <metal/device.h>
#include <metal/irq.h>
#include <metal/utilities.h>
#include <openamp/rpmsg_virtio.h>
#include "platform_info.h"
#include "platform.public.h"
#include "rsc_table.h"

#define IPI_DEV_NAME         "ipi_dev"
#define IPI_BUS_NAME         "generic"
#define IPI_BASE_ADDR        XPAR_XIPIPSU_0_BASE_ADDRESS /* IPI base address*/
#define IPI_CHN_BITMASK      0x01000000 /* IPI channel bit mask for IPI from/to
					   APU */

/* Cortex R5 memory attributes */
#define DEVICE_SHARED		0x00000001U /* device, shareable */
#define DEVICE_NONSHARED	0x00000010U /* device, non shareable */
#define NORM_NSHARED_NCACHE	0x00000008U /* Non cacheable  non shareable */
#define NORM_SHARED_NCACHE	0x0000000CU /* Non cacheable shareable */
#define	PRIV_RW_USER_RW		(0x00000003U<<8U) /* Full Access */




/* IPI information used by remoteproc operations. */
static metal_phys_addr_t ipi_phys_addr = IPI_BASE_ADDR;
struct metal_device ipi_device = {
	.name = "ipi_dev",
	.bus = NULL,
	.num_regions = 1,
	.regions = {
		{
			.virt = (void*)IPI_BASE_ADDR,
			.physmap = &ipi_phys_addr,
			.size = 0x1000,
			.page_shift = -1UL,
			.page_mask = -1UL,
			.mem_flags = DEVICE_NONSHARED | PRIV_RW_USER_RW,
			.ops = {NULL},
		}
	},
	.node = {NULL},
	.irq_num = 1,
	.irq_info = (void *)IPI_IRQ_VECT_ID,
};

static remoteproc_priv rproc_priv = {
	.ipi_name = IPI_DEV_NAME,
	.ipi_bus_name = IPI_BUS_NAME,
	.ipi_chn_mask = IPI_CHN_BITMASK,
};
/* Remoteproc instance */


static remoteproc *platform_create_proc(int proc_index, int rsc_index);

#ifndef nullptr
#define nullptr NULL
#endif

/* External functions */
extern int init_system(void);
extern void cleanup_system(void);

int platform_init(int argc, char *argv[], void **platform)
{
    xil_printf("%s : startup\n", __PRETTY_FUNCTION__ );

    if ( platform == nullptr ) {
        xil_printf("Failed to initialize platform," "NULL pointer to store platform data.\n");
        return -EINVAL;
    }
    /* Initialize HW system components */
    init_system();

    unsigned long proc_id = ( argc >= 2 ) ? strtoul(argv[1], NULL, 0) : 0;
    unsigned long rsc_id  = ( argc >= 3 ) ? strtoul(argv[2], NULL, 0) : 0 ;

    if ( ( *platform = platform_create_proc( proc_id, rsc_id ) ) == nullptr ) {
        xil_printf("%s: Failed to create remoteproc device.\n", __PRETTY_FUNCTION__);
        return -EINVAL;
    }
    return 0;
}


/* RPMsg virtio shared buffer pool */
static remoteproc *platform_create_proc(int proc_index, int rsc_index)
{
    (void) proc_index;
    static const char *backtrace = __PRETTY_FUNCTION__ ;

    /* Register IPI device */
    (void)metal_register_generic_device( & ipi_device );

    extern struct remoteproc_ops zynqmp_r5_a53_proc_ops;
    static remoteproc rproc;
    if (! remoteproc_init( & rproc, & zynqmp_r5_a53_proc_ops, & rproc_priv ) )
        return NULL;

    /* Now we metallize as non-cached/synchronous the memory related with the resources table and the shared */
    /* mmap resource table */
    int rsc_size;
    void *rsc_table = get_resource_table(rsc_index, &rsc_size);

    metal_phys_addr_t tmp = (metal_phys_addr_t)rsc_table;
    remoteproc_mmap( & rproc, &tmp,NULL, rsc_size,NORM_NSHARED_NCACHE|PRIV_RW_USER_RW, &rproc.rsc_io);

    /* mmap shared memory */
    tmp = SHARED_MEM_PA;
    remoteproc_mmap( & rproc, &tmp,NULL, SHARED_MEM_SIZE,NORM_NSHARED_NCACHE|PRIV_RW_USER_RW,NULL);

	/* parse resource table to remoteproc */
    int ret = remoteproc_set_rsc_table( & rproc, rsc_table, rsc_size);
	if (ret) {
        xil_printf("%s: Failed to intialize remoteproc\n", backtrace );
        remoteproc_remove( & rproc);
		return NULL;
	}
    xil_printf("%s: metal data %s($%08x#%d) irq(%3d) masking $%08x  : success\n", backtrace,
               rproc_priv.ipi_name,
               ipi_phys_addr,
               ( ipi_phys_addr - 0xFF300000 ) > 16,
               ( uintptr_t )rproc_priv.ipi_dev->irq_info,
               rproc_priv.ipi_chn_mask );
    return & rproc;
}


/* RPMsg virtio shared buffer pool */
rpmsg_device *platform_create_vdev(void *platform, unsigned int vdev_index, enum VirtIORole role, reset_cb rst_cb, operation_cb operative_cb )
{
    struct remoteproc *rproc = platform;
    struct virtio_device *vdev;
    static const char *backtrace = __PRETTY_FUNCTION__ ;

    struct rpmsg_virtio_device* rpmsg_vdev = metal_allocate_memory(sizeof(*rpmsg_vdev));
    if ( rpmsg_vdev == nullptr)
        return nullptr;
    struct metal_io_region *shbuf_io = remoteproc_get_io_with_pa(rproc, SHARED_MEM_PA);
    if ( shbuf_io == nullptr )
        return nullptr;
    void* shbuf = metal_io_phys_to_virt(shbuf_io, SHARED_MEM_PA + SHARED_BUF_OFFSET);

    xil_printf("%s: dev mapped @ phy-mem $%08x:%4d Kbytes\n",
               backtrace, SHARED_MEM_PA, SHARED_MEM_SIZE >> 10 );

    // wtf : blocking point on freertos.qemu
    if ( ( vdev = remoteproc_create_virtio(rproc, vdev_index, role, rst_cb ) ) == nullptr )
    {
        xil_printf("failed remoteproc_create_virtio\r\n");
        goto err1;
    }

    xil_printf("%s: queues mapped @ phy-mem $%08x:%4d Kbytes\n",
               backtrace, SHARED_MEM_PA + SHARED_BUF_OFFSET, (SHARED_MEM_SIZE - SHARED_BUF_OFFSET) >> 10 );
    static struct rpmsg_virtio_shm_pool shpool;
    rpmsg_virtio_init_shm_pool( & shpool, shbuf, (SHARED_MEM_SIZE - SHARED_BUF_OFFSET));
    if ( rpmsg_init_vdev(rpmsg_vdev, vdev, operative_cb ,shbuf_io, & shpool ) )
    {
        xil_printf("failed rpmsg_init_vdev\r\n");
        goto err2;
    }
    xil_printf("%s: init rpmsg\n", backtrace );
    return rpmsg_virtio_get_rpmsg_device(rpmsg_vdev);

err2:
    remoteproc_remove_virtio(rproc, vdev);
err1:
    metal_free_memory(rpmsg_vdev);
    return NULL;
}


rpmsg_endpoint* platform_create_ept( rpmsg_device *rdev, const char *name, uint32_t start, uint32_t end, epoint_operation epo, epoint_reset epr )
{
    rpmsg_endpoint* ept = ( rpmsg_endpoint* )metal_allocate_memory( sizeof( rpmsg_endpoint ) );
    if( ept )
        if( rpmsg_create_ept( ept, rdev, name, start, end, epo, epr ) != 0 )
            return 0;
    return ept;
}

int platform_xmitt_ept( rpmsg_endpoint* ept, void *payload, size_t len )
{ return rpmsg_send(ept, payload, len) < 0 ? -1 : RPMSG_SUCCESS; }

int platform_poll( void *arg )
{
    remoteproc_priv *prproc = (( remoteproc *)arg)->priv;
    while( 1 )
    {
        /* critical section to synchronize atomic var with the platform isr */
        unsigned int flags = metal_irq_save_disable();
        if ( atomic_flag_test_and_set( & prproc->ipi_nokick ) == false )
        {
			metal_irq_restore_enable(flags);

            /* kick app-domain callbacks + remote processor through metal hal */
            int err = remoteproc_get_notification(( remoteproc *)arg, RSC_NOTIFY_ID_ANY);
            if( err < 0 )
                xil_printf("%s: rproc_notify failed(%d)\n", __PRETTY_FUNCTION__ , err );
            return 0;
        }
        /* wfi -> Wait For Interrupt */
        asm volatile("wfi");
        metal_irq_restore_enable( flags );
	}
	return 0;
}

void platform_release_vdev( rpmsg_device *rpdev, rpmsg_endpoint *epoint)
{
    rpmsg_destroy_ept( epoint );
    metal_free_memory( epoint );
    (void)rpdev;
}

void platform_cleanup(void *platform)
{
    remoteproc *rproc = platform;
	if (rproc)
		remoteproc_remove(rproc);
	cleanup_system();
}
