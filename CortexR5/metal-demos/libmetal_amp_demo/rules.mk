CA53=a53
CA9=a9
CR5=r5


OBJD  =./build.${target}
SRCD ?=./src
INCD ?=./src

RC =\033[0m\033[05;31m\033[01;31m\033[04;31m
BC =\033[00;34m
GC  =\033[00;32m
EOC =\033[0m
AUDITOR=PleaseShowMe

ifeq ($(target), $(CA53))

 CROSS_PREFIX=aarch64-linux-gnu
 CFLAGS = -c -O3 -DARMA53
 LDLIBS += -lpthread

else

ifeq ($(target), $(CR5))

CROSS_PREFIX=armr5-none-eabi

CFLAGS = -c -O3 -DARMR5 -mcpu=cortex-r5 -mfloat-abi=hard  -mfpu=vfpv3-d16
CPPFLAGS = $(CFLAGS)

LDFLAGS  = -mcpu=cortex-r5 -mfloat-abi=hard -mfpu=vfpv3-d16
LDFLAGS += -Wl,-T -Wl,./src/lscript.ld -L./dependencies/prebuild

LDLIBS  = -Wl,--start-group,-lxil,-lgcc,-lc,--end-group
LDLIBS += -Wl,--start-group,-lxil,-lmetal,-lgcc,-lc,--end-group
LDLIBS += -Wl,--start-group -lgcc -lc -lm -lrdimon -lstdc++ -Wl,--end-group

endif

endif

CXX = $(CROSS_PREFIX)-g++
CPP = $(CROSS_PREFIX)-gcc
ELFSIZE = $(CROSS_PREFIX)-size

CPPFLAGS += --std=c++17 -Wno-write-strings -O3
CFLAGS += -I./dependencies/include/bsp -I$(SRCD)

CPP_OBJECTS = $(addsuffix .o, $(CPP_OBJS) )
C_OBJECTS   = $(addsuffix .o, $(C_OBJS) )
APP_OBJS    = $(CPP_OBJECTS) $(C_OBJECTS)

INSTALL_ARTEFACT = $(basename $(APP) ).$(shell git rev-parse --verify --short HEAD ).elf

vpath %.o ${OBJD}
vpath %.c ${SRCD}
vpath %.cpp ${SRCD}

.PHONY: all clean
all: $(AUDITOR) $(OBJD) $(APP)

$(APP): $(APP_OBJS)  
	@printf "Linking : $(GC)$@$(EOC)\n"
	@$(CPP) $(LDFLAGS) -o $@ $(addprefix ${OBJD}/,$(APP_OBJS)) $(LDLIBS)
	@$(ELFSIZE) $@  | tee $@.$(shell git rev-parse --verify --short HEAD ).size

$(C_OBJECTS): %.o : %.c
	@printf "Compile : $(BC)$@$(EOC)\n"
	@$(CPP) $(CFLAGS) -o ${OBJD}/$@ $<

$(CPP_OBJECTS): %.o : %.cpp
	@printf "Compile : $(BC)$@$(EOC)\n"
	@$(CXX) $(CPPFLAGS) -o ${OBJD}/$@ $<

$(OBJD):
	@[ ! -d "${OBJD}" ] && mkdir -p $(OBJD);

$(AUDITOR):
	@printf "\n`date`\n$(BC)$(APP)$(EOC) deployment on $(GC)$(shell $(CPP) -dumpmachine).$(target)$(EOC)\n\n"

install: $(APP) $(INSTALL_ARTEFACT)
	@firmware.installer.sh ${INSTALL_ARTEFACT}

$(INSTALL_ARTEFACT): $(APP)
	 @cp $< $@

clean:
	@rm -rf $(APP) $(OBJD)/*.o
