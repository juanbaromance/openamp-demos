/*
 * Copyright (c) 2017, Xilinx Inc. and Contributors. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of Xilinx nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 /*****************************************************************************
 * atomic_shmem_demod.c - Shared memory atomic operation demo
 * This task will:
 *  1. Get the shared memory device I/O region.
 *  2. Get the IPI device I/O region.
 *  3. Register IPI interrupt handler.
 *  4. Wait for the APU to kick IPI to start the demo
 *  5. Once notification is received, start atomic add by
 *     1 for 5000 times over the shared memory
 *  6. Trigger IPI to notify the remote it has finished calculation.
 *  7. Clean up: Disable IPI interrupt, deregister the IPI interrupt handler.
 */

#include <metal/shmem.h>
#include <metal/atomic.h>
#include <metal/device.h>
#include <metal/io.h>
#include <sys/time.h>
#include <stdio.h>
#include "common.h"
#include "sys_init.h"

#define ATOMIC_INT_OFFSET 0x0 /* shared memory offset for atomic operation */
#define ITERATIONS 5000

using metal_device = struct metal_device;
using metal_io_region = struct metal_io_region;
static atomic_flag remote_nkicked;

static int ipi_irq_handler (int vect_id, void *priv)
{
    (void)vect_id;
    metal_io_region *ior = (metal_io_region *)priv;
    if ( ior == nullptr )
        return METAL_IRQ_NOT_HANDLED;

    constexpr const uint32_t ipi_mask = IPI_MASK;
    if ( metal_io_read32( ior, IPI_ISR_OFFSET) & ipi_mask )
    {
        metal_io_write32( ior, IPI_ISR_OFFSET, ipi_mask);
        atomic_flag_clear( & remote_nkicked );
        return METAL_IRQ_HANDLED;
    }
    return METAL_IRQ_NOT_HANDLED;
}

static int atomic_add_shmemd( metal_io_region *ipi_io, metal_io_region *shm_io, uint32_t ipi_mask = IPI_MASK )
{
    constexpr const char *backtrace = __FUNCTION__ ;
    LPRINTF("%s: waiting apu.kick from : 0x%08x\n" , backtrace, ipi_mask );
    atomic_int *shm_int = ( atomic_int * )metal_io_virt( shm_io, ATOMIC_INT_OFFSET );

    wait_for_notified( & remote_nkicked );
    for ( size_t i = 0; i < ITERATIONS; i++ )
        atomic_fetch_add( shm_int, 1 );
    metal_io_write32( ipi_io, IPI_TRIG_OFFSET, ipi_mask);

    LPRINTF("%s : apu kicked : expected state %u, actual: %u\n",
            backtrace, (unsigned int)(ITERATIONS << 1), *shm_int );
    return 0;
}

#include <array>
#include <metal/sleep.h>
#include <bitset>

#ifdef __cplusplus
extern "C" {
#endif

int atomic_shmem_demod()
{
    print_demo(": Setting up metal-demo on atomic operation over shared memory");
    std::array<metal_device*,2> m_devices = { shm_dev, ipi_dev };
    std::array<metal_io_region*, m_devices.size()> m_io;

    auto error = [](const char *backtrace, int error)
    {
        LPERROR("%s(%d)", backtrace, error );
        return error;
    };

    auto ipi_setup = []( metal_io_region * ior, const int irq, const uint32_t mask = IPI_MASK )
    {
        metal_io_write32(ior, IPI_IDR_OFFSET, mask );
        metal_io_write32(ior, IPI_ISR_OFFSET, mask );
        if( int ret = metal_irq_register( irq, ipi_irq_handler, ior); ret < 0 )
            return ret;
        metal_irq_enable( irq );
        atomic_flag_clear( & remote_nkicked);
        atomic_flag_test_and_set( & remote_nkicked);
        metal_io_write32(ior, IPI_IER_OFFSET, mask );
        return 0;
    };

    auto ipi_cleanup = [](metal_io_region * ior, const int irq, const uint32_t mask = IPI_MASK )
    {
        metal_io_write32(ior, IPI_IDR_OFFSET, mask);
        metal_irq_disable(irq);
        metal_irq_unregister(irq);
    };

    size_t i = 0;
    for( auto & dev : m_devices )
    {
        if( dev )
            if( ( m_io[ i++ ] = metal_device_io_region( dev, 0 ) ) )
                continue;
        return error( "mapping failure :", -ENODEV );
    }

    metal_io_region *ipi_io;
    const int irq_vector = ( intptr_t )ipi_dev->irq_info;
    int ret = ipi_setup( ipi_io = m_io[1], irq_vector );
    bool enabled = std::bitset<32>( metal_io_read32( ipi_io, IPI_IMR_OFFSET ) ).test(24) == 0;

    LPRINTF("%s: ipi.phy(0x%08x).irq%2d(0x%0x) : (%s)(%s) -> 0x%08x", __FUNCTION__ ,
            metal_io_phys( ipi_io, 0),
            irq_vector, irq_vector,
            ( ret == 0 ) ? "registered" : "isr.register : failured" ,
            enabled ? "enabled" : "disabled", IPI_MASK );

    ret = ( enabled ) ? atomic_add_shmemd( ipi_io, m_io[ 0 ] ) : -1;
    ipi_cleanup( ipi_io, irq_vector );
    return ret;

}

#ifdef __cplusplus
}
#endif
