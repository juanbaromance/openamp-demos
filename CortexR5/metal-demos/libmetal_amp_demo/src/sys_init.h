#pragma once
#include "platform_config.h"

#ifdef __cplusplus
extern "C" {
#endif

int sys_init();
void sys_cleanup();
#ifdef __cplusplus
}
#endif
