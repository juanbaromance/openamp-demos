#!/bin/bash
# R5.0 baremetal standalone emulation specifications

C4='\033[00;34m'
C5='\033[00;32m'
RC='\033[0m'
auditor="${C5}`basename $0`:${RC}"
ToolChainPrefix="armr5-none-eabi"

elfimage="$1"
[ -z "$elfimage" ] && elfimage="echoTesting.elf";

simulator="./qemu-system-aarch64"
machine="-M arm-generic-fdt"
firmware="-device loader,file=./$elfimage,cpu-num=4"

display="-serial mon:stdio -display none -nographic"
dtb="-hw-dtb ./zynqmp-qemu-arm.dtb"

release_reset="-device loader,addr=0xff5e023c,data=0x80008fde,data-len=4 -device loader,addr=0xff9a0000,data=0x80000218,data-len=4 "

gdb_port="1440"
gdb_stub="-gdb tcp::${gdb_port}"

export LANG=""
date 
printf "${auditor} Loading ${C4}$elfimage${RC} on ZynQ.arquitecture ${C4}CR5.0${RC}\nctrl-a c :interacts with QEMU console, ctrl-a x :quit emulation\n"

printf "${auditor} `${ToolChainPrefix}-readelf -S ${elfimage}  | grep AX`\n\n"

read -t 3

${simulator} ${machine} ${dtb} ${display} ${firmware} ${release_reset}
