#!/bin/sh
C4='\033[00;34m'
C5='\033[00;32m'
BD='\033[00;1m'
RC='\033[0m'
auditor="${C5}`basename $0`:${RC}"

export LANG=""
printf "\n"
date

elf_image=$1
[ -z "$elf_image" ] && elf_image="image_echo_test";

target="remoteproc0"
rproc_path="/sys/class/remoteproc/${target}"
current_firmware="`cat ${rproc_path}/firmware`"
[ ! "$?" == 0 ] && printf "${auditor} <${C4}${target}${RC}> undefined remote processor structure ${C4}@`hostname`${RC}\n\n" && exit -1;

if [ "$1" == "recycle" ]; then
  printf "${auditor} ReCycling <${C4}${target}:${current_firmware}${RC}> processor\n"
  echo stop > ${rproc_path}/state
  echo start > ${rproc_path}/state
  exit 0;
fi

[ ! -f "/lib/firmware/${elf_image}" ] && printf "${auditor} ${elf_image} No such file\n" && exit -2;
printf "${auditor} firmware on ${C4}${target}${RC}(/lib/firmware/) -> ${C5}`cat ${rproc_path}/firmware`${RC}\n"
printf "now re-addressed to -> ${C5}${elf_image}${RC}\n"

[ "`cat /sys/class/remoteproc/remoteproc0/state`" == "running" ] && echo stop > ${rproc_path}/state;

echo ${elf_image} > ${rproc_path}/firmware
echo start > ${rproc_path}/state
tmp_file="./openamp.`whoami`.log"
hostname="`hostname`"
grep virtio /var/log/messages | tail -15 | sed 's/user.info kernel//g' > ${tmp_file}

printf "`tail -7 ${tmp_file}`\n"



