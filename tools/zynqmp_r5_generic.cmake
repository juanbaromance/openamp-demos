set (CMAKE_SYSTEM_PROCESSOR "arm" CACHE STRING "")
set (MACHINE        "zynqmp_r5" CACHE STRING "")
set (CROSS_PREFIX   "armr5-none-eabi-" CACHE STRING "")
set (CMAKE_C_FLAGS  "-mfloat-abi=hard -mfpu=vfpv3-d16 -mcpu=cortex-r5 -Os -fmessage-length=0" CACHE STRING "")
set (CMAKE_CXX_FLAGS "-mfloat-abi=hard -mfpu=vfpv3-d16 -mcpu=cortex-r5 -Os -fmessage-length=0" CACHE STRING "")

set ( pwd ${CMAKE_CURRENT_SOURCE_DIR} )
set( Platform BMCR5 )
ADD_DEFINITIONS( -D${Platform}__ -DBareMetal__ -DARMR5 )
include_directories ( ${pwd} ./inc ./dependencies/include ./dependencies/include/bsp )

cmake_policy(SET CMP0048 NEW)
# set ( CMAKE_GENERATOR make )
# set ( CMAKE_SYSTEM_NAME "Generic"             CACHE STRING "")
set ( CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER CACHE STRING "")
#set ( CMAKE_FIND_ROOT_PATH_MODE_LIBRARY NEVER CACHE STRING "")
#set ( CMAKE_FIND_ROOT_PATH_MODE_INCLUDE NEVER CACHE STRING "")
set ( CMAKE_C_COMPILER   ${CROSS_PREFIX}gcc )
set ( CMAKE_CXX_COMPILER ${CROSS_PREFIX}g++ )

SET (CMAKE_C_COMPILER_WORKS 1)
SET (CMAKE_CXX_COMPILER_WORKS 1)

string ( CONCAT prelnk_spec
    "--specs=nosys.specs --specs=rdimon.specs -specs=${pwd}/Xilinx.spec -s -Os "
    "-Wl,-build-id=none "
    "-Wl,-T -Wl,${pwd}/lscript.ld " ${Target_deps} )

set ( CMAKE_EXE_LINKER_FLAGS ${prelnk_spec} )

# vim: expandtab:ts=2:sw=2:smartindent
