#!/bin/bash
# A9.1 baremetal standalone emulation specifications

C4='\033[00;34m'
C5='\033[00;32m'
RC='\033[0m'
auditor="${C5}`basename $0`:${RC}"
ToolChainPrefix="arm-none-eabi"

elfimage="$1"
[ -z "$elfimage" ] && exit -1;

simulator="./qemu-system-aarch64"
machine="-M arm-generic-fdt-7series"
firmware="-device loader,file=./$elfimage,cpu-num=1"

# connect UART0 to monitor stdio and ground UART1 
serial="-serial mon:stdio -serial /dev/null"
display="-display none"
dtb="-dtb system.dtb"

chipset_setup=""
#network="-net nic -net tap,ifname=tap1,downscript=no"
network="-netdev tap,id=eth1,ifname=tap1"
log_network="-object filter-dump,id=f1,netdev=eth1,file=dump.dat"

gdb_port="1440"
gdb_stub="gdb tcp::${gdb_port}"
gdb_setup=""
[ "$2" == "debug" ] && gdb_setup="-${gdb_stub}";

export LANG=""
printf "\n"
date 
printf "${auditor} Loading ${C4}$elfimage${RC} on ZynQ.arquitecture ${C4}CA9.1${RC}\n ${gdb_setup}\n"
printf "ctrl-a c :interacts with QEMU console, ctrl-a x :quits emulation\n \
 xp 0x......     :prints physical\n \
  x 0x......     :prints virtual \n \
  info cpus      :shows available CPUs\n \
  info registers :dumps CPU registers\n\n"

printf "${auditor} `${ToolChainPrefix}-readelf -h ${elfimage} | grep Entry`\n\n"

read -t 2

${simulator} ${machine} ${dtb} ${serial} ${display} ${firmware} ${chipset_setup} ${gdb_setup}

# ${network} ${log_network} 
