#!/bin/bash

device="tap1"
TapIpAddress="10.0.2.16"

printf "# `date` Starting up ${device} QEmu/TAP environment..\n\n"

if [ -z "`ip link | grep ${device}`" ]; then
   printf "# Setting ${device} networking device on address ${TapIpAddress}\n\n"
   sudo tunctl -t ${device} -u `whoami`
   sudo ifconfig ${device} ${TapIpAddress}
fi

ip address show ${device}

if [ -z "`ps aux | grep udhcpd | grep root`" ]; then
   printf "# Setting Up uDHCP server\n\n"
   sudo udhcpd -S
fi

if [ "$1" == "boot" ]; then
   printf "# Booting QEMU-ZynQ...\n\n"
   source /opt/development/zynq/petalinux-sdk/2019.2/settings.sh
   petalinux-boot --qemu --kernel --qemu-args "-net nic -net tap,ifname=${device}"
fi