#!/bin/bash
# Target tweaker ssh/openamp

C4='\033[00;34m'
C5='\033[00;32m'
BD='\033[00;1m'
RC='\033[0m'
auditor="${C5}`basename $0`:${RC}"

export LANG=""
printf "\n"
date

uZedQemu="uZedQemu"
u96="u96"
uZedChallengeX="uZedChallengeX"
u96ChallengeX="u96ChallengeX"
popov="uZedPopov"

[ $# -ne 1 ] && {
    printf "${auditor} : missed environment\n[ ";
    for option in ${uZedQemu} ${u96} ${uZedChallengeX} ${u96ChallengeX} ${popov}  
    do
	printf "${option}|";
    done
    printf " ]\n\n";
    exit;
}

zynqBoard=$1

case ${zynqBoard} in
    ${uZedQemu}|${uZedChallengeX}|${popov})
	host="10.0.2.15"
	[ ${zynqBoard} == ${popov} ] && host="192.168.0.11";
	[ ${zynqBoard} == ${uZedChallengeX} ] && host="192.6.1.131";
	sshCommand="ssh"
	scpCommand="scp"
	;;

    ${u96}|${u96ChallengeX})
	host="localhost"
	forwardPort="1440"
	sshCommand="ssh -p ${forwardPort}"
	scpCommand="scp -P ${forwardPort}"
	printf "${auditor} Setting up ${C4}ssh@${host}${RC} for zynq.${C4}${zynqBoard}${RC}\n"
	# mkdir .ssh;vi /etc/default/dropbear; /etc/init.d/dropbear stop; /etc/init.d/dropbear start"
	;;
    *)
	printf "environment \"${zynqBoard}\" : Unknown, nothing to do\n";
	exit;
	;;
esac

sshDir="~/.ssh"
printf "${auditor} Setting up ${C4}rootfs@${host}${RC} for zynq.${C4}${zynqBoard}${RC}\n"

${sshCommand} root@${host} '[ ! -d ${sshDir} ] && mkdir ${sshDir}'
cat ~/.ssh/id_rsa.pub | ${sshCommand} root@${host} 'cat >> .ssh/authorized_keys'

${sshCommand} root@${host} 'ls -al -h'
[ "$?" == 0 ] && { 
    printf "\n${auditor}${C4}${zynqBoard}(${host})${RC} ssh setup: ${C5}success${RC}\nopenAMP setup can be applied\n"; 
    OpenAMPDir="/opt/shared/bin/openAMP"
    ${sshCommand} root@${host} "mount -o remount,rw /;mkdir -p ${OpenAMPDir}"
    oamp_loader="openamp.loader.sh"

    ${scpCommand} ${oamp_loader} root@${host}:${OpenAMPDir}
    ${scpCommand} ${oamp_loader} root@${host}:/lib/firmware
    
    printf "Initialising ${C4}${zynqBoard}(${host})${RC} openAMP framework\n"; 
    ${sshCommand} root@${host} ${OpenAMPDir}/${oamp_loader} recycle
}
