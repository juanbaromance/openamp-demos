#!/bin/bash
# Loads on the Zynq target openamp either linux or baremetal artefacs
C4='\033[00;34m'
C5='\033[00;32m'
BD='\033[00;1m'
RC='\033[0m'
auditor="${C5}`basename $0`:${RC}"

export LANG=""
printf "\n"
date

[ $# -ne 1 ] && printf "${auditor} : missed elf image\n" && exit -1;
target=$1
[ ! -e ${target} ] && printf "${auditor} : ${target} no such file\n" && exit -1;

uZedQemu="uZed"
u96="u96"
uZedChallengeX="uZed.ChallengeX"
u96ChallengeX="u96.ChallengeX"
popov="uZedPopov"


zynqBoard=${uZedQemu}
bare_metal="n"
remote_path="/opt/shared/bin/openAMP"

if [ "${target##*.}" = "elf" ]; then
    remote_path="/lib/firmware"
    bare_metal="y"
    [ ! -z "`readelf ${target} -A | grep Realtime`" ] && zynqBoard="u96";
else
    [ ! -z "`readelf ${target} -h | grep AArch64`" ] && zynqBoard="u96";
fi

sshCommand="ssh"
scpCommand="scp"

case ${zynqBoard} in
    ${uZedQemu}|${uZedChallengeX}|${popov})
	[ ${zynqBoard} == ${uZedChallengeX} ] && host="192.6.1.131"
	[ ${zynqBoard} == ${uZedQemu}       ] && host="10.0.2.15"
	[ ${zynqBoard} == ${popov}          ] && host="192.168.0.11"
	;;

    ${u96}|${u96ChallengeX})
	host="localhost"
	forwardPort=1440
	sshCommand="${sshCommand} -p ${forwardPort}"
	scpCommand="${scpCommand} -P ${forwardPort}"
	;;
    
    *)
	printf "environment \"${zynqBoard}\" : Unknown, nothing to do\n";
	# todo : print default environments
	exit -1;
	;;
esac

printf "\n${auditor} Installing ${C4}${target}${RC} on ${host}:${remote_path}(${C4}${zynqBoard}${RC})\n"

[ ! -e ${target} ] && { printf "${auditor} ${C4}${target}${RC} : no such file\n\n"; exit; }

remount="mount -o remount,rw /"
${sshCommand} root@$host "[ -d "${remote_path}" ] && echo yes"
${sshCommand} root@$host "[ ! "`cat /proc/mounts | grep /root | awk '{print substr($4,1,2)}'`" == "rw" ] && echo ${remount}"

${scpCommand} ${target} root@${host}:${remote_path};
[  $? = 1 ] && exit -1;

[ "${bare_metal}" == "y" ] && 
{
   ${sshCommand} root@$host "ls -al ${remote_path};${remote_path}/openamp.loader.sh `basename ${target}`"
   exit 0 ;
}

${sshCommand} root@${host} ${remote_path}/openamp.loader.sh recycle
exit 0;
