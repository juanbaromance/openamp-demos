### Building Tweaks 

The **open-amp** amd **libmetal** packages are implemented with **cmake**, apply below recipe on the building directories afterwards sources installation in your host

```
cmake ../src/libmetal/ -DCMAKE_TOOLCHAIN_FILE=../src/libmetal/cmake/platforms/toolchain.cmake
cmake ../src/open-amp/ -DCMAKE_TOOLCHAIN_FILE=../src/open-amp/cmake/platforms/toolchain.cmake
```

Before making anything, assure a proper overloading of your execution path with the Xilinx SDK R5/A9 baremetal toolchains

```
jb@popov $ export PATH=$PATH:*YourSDKRoot*/2019.2/tools/xsct/gnu/armr5/lin/gcc-arm-none-eabi/bin/
jb@popov $ export PATH=$PATH:*YourSDKRoot*/2019.2/tools/xsct/gnu/aarch32/lin/gcc-arm-none-eabi/bin/
```

The **prebuilt**  directory nemonic contains the headers/static libraries/elf of the pre-compiled packages ready to be used. The .mss (microprocessor software specification) provides the specific versions for the every component of the .bsp (board support package)
