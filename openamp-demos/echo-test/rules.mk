CA53=a53
CA9=a9

AUDITOR_LEVEL ?= Silent
OBJD  =./build.${target}
SRCD ?=./src
INCD ?=./src

RC =\033[0m\033[05;31m\033[01;31m\033[04;31m
BC =\033[00;34m
GC  =\033[00;32m
EOC =\033[0m
AUDITOR=PleaseShowMe

ifeq ($(target), $(CA53))

 CROSS_PREFIX=aarch64-linux-gnu
 CFLAGS = -c -O3 -DARMA53
 LDLIBS =+= -lpthread
 CPPFLAGS = $(CFLAGS)

else

ifeq ($(target), $(CA9))

 CROSS_PREFIX=arm-xilinx-linux-gnueabi
 SDKTARGETSYSROOT=/opt/petalinux/2019.2/sysroots/cortexa9t2hf-neon-xilinx-linux-gnueabi
 CFLAGS = -c -pipe -feliminate-unused-debug-types -O3 -DARMCA9
 CFLAGS += -march=armv7-a -mthumb -mfpu=neon -mfloat-abi=hard -mcpu=cortex-a9 
 CFLAGS += --sysroot=$(SDKTARGETSYSROOT)
 LDFLAGS= -march=armv7-a -mthumb -mfpu=neon -mfloat-abi=hard -mcpu=cortex-a9 --sysroot=$(SDKTARGETSYSROOT) -pipe -Wl,--hash-style=gnu -Wl,--as-needed -rdynamic
 CPPFLAGS = $(CFLAGS) --std=c++17

endif

endif

CPP_OBJECTS = $(addsuffix .o, $(CPP_OBJS) )
C_OBJECTS   = $(addsuffix .o, $(C_OBJS) )
APP_OBJS    = $(CPP_OBJECTS) $(C_OBJECTS)

LDLIBS += -lpthread -lm
INSTALL_ARTEFACT = $(basename $(APP) ).$(shell git rev-parse --verify --short HEAD ).x

CXX = $(CROSS_PREFIX)-g++
CPP = $(CROSS_PREFIX)-gcc

.PHONY: all clean
all: $(AUDITOR) $(OBJD) $(APP)

$(APP): $(APP_OBJS)
	@printf "Linking : $(GC)$@$(EOC)\n"
	$(CXX) $(LDFLAGS) -o $@ $(addprefix ${OBJD}/,$(APP_OBJS)) $(LDLIBS)

vpath %.o ${OBJD}
vpath %.c ${SRCD}
vpath %.cpp ${SRCD}

$(C_OBJECTS): %.o : %.c
	@printf "Compile : $(BC)$@$(EOC)\n"
ifeq ($(AUDITOR_LEVEL), Silent)
	@$(CPP) $(CFLAGS) -o ${OBJD}/$@ $<
else
	$(CPP) $(CFLAGS) -o ${OBJD}/$@ $<
endif

$(CPP_OBJECTS): %.o : %.cpp
	@printf "Compile : $(BC)$@$(EOC)\n"
ifeq ($(AUDITOR_LEVEL), Silent)
	@$(CXX) $(CPPFLAGS) -o ${OBJD}/$@ $<
else
	$(CXX) $(CPPFLAGS) -o ${OBJD}/$@ $<
endif

install: $(APP) $(INSTALL_ARTEFACT)
	@./firmware.installer.sh $(INSTALL_ARTEFACT)

$(INSTALL_ARTEFACT): $(APP)
	 @cp $< $@

$(AUDITOR):
	@printf "\n`date`\n$(BC)$(APP)$(EOC) deployment on $(GC)$(shell $(CPP) -dumpmachine).$(target)$(EOC)\n\n"

clean:
	@rm -rf $(APP) $(OBJD)/*.o
