#include "helper.h"
#include <iostream>
#include <cstring>

int dump( const std::string &  backtrace, bool abort )
{
    int err = -1;
    auto & stream = abort ? std::cerr : std::cout ;
    if ( abort == true )
    {
        ( stream << backtrace << " errno(" << errno << ")( " << strerror(errno) << " )\n\n" ).flush();
        exit( err );
    }
    stream << backtrace << "\n";
    return err = 0;
}
