#include "end_point.h"
#include "helper.h"
#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <time.h>
#include <fcntl.h>
#include <string.h>
#include <linux/rpmsg.h>
#include <iostream>
#include <iomanip>

class cProperty
{
public:
    cProperty( const std::string & path, const std::string & value, const std::string & backtrace ="" )
    {
        using namespace  std;
        auto error = [&]( const string log ){ return dump( backtrace + log );  };
        if ( int fd = open(path.c_str(), O_WRONLY); fd >= 0 )
        {
            if( write( fd, value.c_str(), value.size() + 1 ) >= 0 )
                return;
            error( "\n" + value + " write : failure -> " + path );
        }
        error( ": open failure " + path );
    }
    ~cProperty(){ close( fd ); }
private:
    int fd;
};

cRPEndPoint::cRPEndPoint(const char *name) : channel_name( name ) {}

cRPEndPoint &cRPEndPoint::deploy(const std::string &virtio_name )
{
    static constexpr const char* backtrace = __PRETTY_FUNCTION__ ;
    using namespace std;
    rpmsg_endpoint_info eptinfo;
    strcpy( eptinfo.name, channel_name.c_str() );
    eptinfo.src = 0;
    eptinfo.dst = 0xFFFFFFFF;

    auto virtio = rpmsg_fd( virtio_name );
    int tmp = std::get<0>(virtio);
    if( ioctl( tmp , RPMSG_CREATE_EPT_IOCTL, & eptinfo ) )
    {
        string error{ string{backtrace} + "\n Failed to create endpoint "};
        error += virtio_name + "(" + to_string(tmp) +  ")" + eptinfo.name;
        dump( error );
    }
    close( tmp );

    std::string path_name = endpoint_path(  std::get<1>(virtio), eptinfo.name );
    if( ( fd = open( ( std::string("/dev/") + path_name ).c_str(), O_RDWR | O_NONBLOCK) ) < 0 )
        dump( "failure open device: " + path_name );
    return *this;
}

#include <filesystem>
int cRPEndPoint::iface(){ return fd; }

cRPEndPoint::~cRPEndPoint()
{
    using namespace std;
    const string unbind{ string{RPMSG_BUS_SYS} + "/drivers/rpmsg_chrdev/unbind" };
    cProperty( unbind, virtio_name, __PRETTY_FUNCTION__ );
    close( fd );
}

cRPEndPoint::rpmsg_descriptor cRPEndPoint::rpmsg_fd(const std::string &virtio_name)
{
    using namespace std;
    static constexpr const char* backtrace = __PRETTY_FUNCTION__ ;
    auto error = [&]( const string log ){ dump( backtrace + log ); return std::make_tuple( -1, log );  };
    namespace fs = std::filesystem;

    binding( this->virtio_name = virtio_name );
    const string path{ string{RPMSG_BUS_SYS} + "/devices/" + virtio_name + "/rpmsg"};
    DIR* dir = opendir( path.c_str() );
    if ( dir == nullptr )
        return error( ": No rpmsg char dev file is found : " + path );

    struct dirent *ent;
    while (( ent = readdir(dir) ) )
    {
        const char *tmp = "rpmsg_ctrl";
        if ( ! strncmp( ent->d_name, tmp, strlen(tmp) ) )
        {
            const string path{ "/dev/" + string{ent->d_name}};
            int fd = open( path.c_str(), O_RDWR | O_NONBLOCK);
            if ( fd < 0 )
                return error( " : opening path " + path );
            return std::make_tuple( fd, string{ent->d_name} );
        }
    }
    return error( " : No entry rpmsg char dev file is found @ " + path );
}

int cRPEndPoint::binding(const std::string &device)
{
    using namespace std;
    constexpr const char* backtrace = __PRETTY_FUNCTION__ ;
    string tmp{ string{RPMSG_BUS_SYS} + "/devices/" + device };
    if ( access( tmp.c_str(), F_OK ) )
        dump( string(backtrace) + "\n" + tmp );
    const string override{ string{RPMSG_BUS_SYS} + "/devices/" + device + "/driver_override"};
    cProperty( override, "rpmsg_chrdev", backtrace );
    const string bind{ string{RPMSG_BUS_SYS} + "/drivers/rpmsg_chrdev/bind" };
    cProperty( bind, device, backtrace );
    return 0;
}

std::string cRPEndPoint::endpoint_path(const std::string &channel_name, const std::string &ept_name)
{
    using namespace std;
    static constexpr const char* backtrace = __PRETTY_FUNCTION__ ;

    auto error = [&]( const string log ){ dump( backtrace + log ); return ""; };
    for ( size_t i = 0; i < 128; i++)
    {
        string tmp{"/sys/class/rpmsg/" + channel_name + "/rpmsg" + to_string(i) + "/name"};
        if ( access( tmp.c_str(), F_OK) < 0 )
            continue;
        if ( FILE *fp =  fopen( tmp.c_str(), "r"); fp )
        {
            char ept_probe[64];
            fgets( ept_probe, sizeof( ept_probe ), fp);
            fclose(fp);
            if( ept_name.compare(  0, ept_name.size(), ept_probe ) )
                return "rpmsg" + std::to_string( i );
        }
    }
    return error( string{": unable to get rpmsg endpoint entry for channel named as: "} + ept_name );
}


