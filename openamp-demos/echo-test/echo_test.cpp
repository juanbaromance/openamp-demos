/*
 * echo_test.c
 *
 * Created on: Oct 4, 2014
 *      Author: etsam
 * Reviews:
 * March 2020 jb applies c++17 Refactor
 */

/*
 * Test application that data integraty of inter processor
 * communication from linux userspace to a remote software
 * context. The application sends chunks of data to the
 * remote processor. The remote side echoes the data back
 * to application which then validates the data returned.
 */


#include <iostream>
#include <iomanip>
#include <thread>
#include <unistd.h>
#include <tuple>
#include <cstring>
#include <iostream>
#include <chrono> 
#include <ctime> 

#include "helper.h"
#include "end_point.h"

static bool endPointTesting(const int fd , const int ntimes = 1 );

int main(int argc, char *argv[])
{   
    using namespace std; 
    
    std::cout << "\n";
    auto timenow = 
    chrono::system_clock::to_time_t(chrono::system_clock::now()); 
    cout << ctime(&timenow) << endl; 

    if ( int ret = system("modprobe rpmsg_char"); ret < 0 )
        dump("Failed loading kernel with rpmsg_char");
    static constexpr const char* channel_name = "rpmsg-openamp-demo-channel";
    std::string virtio_name{"virtio0." + std::string{ channel_name } + ".-1.0"};
    std::thread( endPointTesting, cRPEndPoint( channel_name ).deploy( virtio_name ).iface(), 1 ).join();
    return 0;
}


#include <cstdio>
bool endPointTesting( const int fd, const int ntimes )
{
    using namespace std;
    static const string backtrace = __PRETTY_FUNCTION__ ;
    struct _payload {
        unsigned long id;
        unsigned long port_size;
        uint8_t data[];
    };
    static constexpr uint8_t pattern = 0b01011010;
    static constexpr uint8_t szOfheader = sizeof( decltype( _payload::id ) ) + sizeof( decltype( _payload::port_size ) );

    enum Numerology {
        RPMSG_HEADER_LEN = 16,
        // #define MAX_RPMSG_BUFF_SIZE (512 - RPMSG_HEADER_LEN)
        MAX_RPMSG_BUFF_SIZE = (64 - RPMSG_HEADER_LEN),
        PAYLOAD_MIN_SIZE = 1,
        PAYLOAD_MAX_SIZE =	(MAX_RPMSG_BUFF_SIZE - 24),
        NUM_PAYLOADS = PAYLOAD_MAX_SIZE/PAYLOAD_MIN_SIZE,
    };

    struct _payload *tx_pack = (struct _payload *)malloc( szOfheader+ PAYLOAD_MAX_SIZE);
    struct _payload *rx_pack = (struct _payload *)malloc( szOfheader+ PAYLOAD_MAX_SIZE);

    if (tx_pack == 0 || rx_pack == 0 )
        return dump( backtrace + "ERROR: Failed to allocate memory for payload", abort_ );

    size_t err_cnt = 0;
    for ( int j = 0; j < ntimes; ++j )
    {
        for ( int i = 0, size = PAYLOAD_MIN_SIZE; i < NUM_PAYLOADS; ++i, ++size )
        {
            tx_pack->id = i;
            memset( tx_pack->data, pattern, tx_pack->port_size = size);

            if ( ssize_t bytes_sent = write(fd, tx_pack, szOfheader + size ); bytes_sent <= 0 )
            {
                std::ostringstream oss;
                oss << backtrace << ": tx-payload.id( "<< tx_pack->id << "failured as err(" << bytes_sent << ")";
                oss << " : test aborted";
                dump( oss.str(), info_ );
                break;
            }

            // usleep(1000);

            memset( rx_pack, 0, sizeof( struct _payload ) );
            int bytes_rcvd = read( fd, rx_pack, szOfheader + PAYLOAD_MAX_SIZE );
            int missed = 0;
            while ( bytes_rcvd <= 0 )
            {
                usleep(1000);
                bytes_rcvd = read( fd, rx_pack, szOfheader + PAYLOAD_MAX_SIZE );
                ++missed;
            }

            printf( "payload.id(%4ld)( ack.sized as %4d vs %4ld )( %d )\r\n",
                   rx_pack->id, bytes_rcvd, tx_pack->port_size + szOfheader, missed );

            /* Validate data buffer integrity. */
            for ( size_t k = 0; k < rx_pack->port_size; ++k )
            {
                if ( rx_pack->data[k] == pattern )
                    continue;
                std::ostringstream oss;
                oss << backtrace << " warning : Data corruption at index(" << std::dec << k << ")"
                    << "pattern(" << std::hex << pattern << ") vs ack(" << rx_pack->data[k];
                dump( oss.str(), info_ );
                err_cnt++;
                break;
            }
        }
        ( cout << __PRETTY_FUNCTION__ << "Round " << j << " err_count(" << err_cnt << ")\n" ).flush();
    }

    free( tx_pack );
    free( rx_pack );
    return 0;
}
