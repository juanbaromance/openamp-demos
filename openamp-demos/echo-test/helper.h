#pragma once

#include <string>

enum {
    abort_ = true,
    info_ = ! abort_
};

int  dump( const std::string & backtrace, bool abort = abort_ );
