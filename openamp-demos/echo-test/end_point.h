#pragma once

#define RPMSG_BUS_SYS "/sys/bus/rpmsg"


#include <string>
class cRPEndPoint {
private:

public:
    cRPEndPoint( const char* name );
    cRPEndPoint & deploy( const std::string & virtio_name );
    int iface();
    ~cRPEndPoint();

private:
    using rpmsg_descriptor = std::tuple<int,std::string> ;
    rpmsg_descriptor rpmsg_fd( const std::string & virtio_name );
    int binding( const std::string & device );
    std::string endpoint_path( const std::string & channel_name, const std::string & ept_name );
    std::string channel_name;
    int fd;
    std::string virtio_name;
};
