#pragma once

#ifdef __cplusplus
extern "C" {
#endif

int sys_init();
int sys_cleanup();
void wait_for_interrupt(void);

#ifdef __cplusplus
}
#endif

