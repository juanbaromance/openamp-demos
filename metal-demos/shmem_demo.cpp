/*
 * Copyright (c) 2017, Xilinx Inc. and Contributors. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

 /*****************************************************************************
  * shmem_demo.c
  * This demo demonstrates the use of shared mem. between the APU and RPU.
  * This demo does so via the following steps:
  *
  *  1. Open the shared memory device.
  *  2. Clear the demo control TX/RX available values in shared memory.
  *  3. APU set demo control in shared memory to notify RPU demo has started
  *  4. APU will write message to the shared memory.
  *  5. APU will increase TX avail values in the shared memory to notify RPU
  *     there is a message ready to read.
  *  6. APU will poll the RX avail value in th shared memory to see if RPU
  *     has echoed back the message into the shared memory.
  *  7. When APU knows there is new RX message available, it will read the
  *     RX message from the shared memory.
  *  8. APU will verify the message to see if it matches the one it has sent.
  *  9. Close the shared memory device.
  *
  * Here is the Shared memory structure of this demo:
  * |0    | 4Bytes | DEMO control status shows if demo starts or not |
  * |0x04 | 4Bytes | number of APU to RPU buffers available to RPU |
  * |0x08 | 4Bytes | number of APU to RPU buffers consumed by RPU |
  * |0x0c | 4Bytes | number of RPU to APU buffers available to APU |
  * |0x10 | 4Bytes | number of RPU to APU buffers consumed by APU |
  * |0x14 | 1KBytes | APU to RPU buffer |
  * ... ...
  * |0x800 | 1KBytes | RPU to APU buffer |
  */

#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <errno.h>
#include <string.h>
#include <metal/io.h>
#include <metal/alloc.h>
#include <metal/device.h>
#include "common.h"

/* Shared memory offsets */
#define SHM_DEMO_CNTRL_OFFSET      0x0
#define SHM_TX_AVAIL_OFFSET        0x04
#define SHM_RX_AVAIL_OFFSET        0x0C
#define SHM_TX_BUFFER_OFFSET       0x14
#define SHM_RX_BUFFER_OFFSET       0x800

#define SHM_BUFFER_SIZE          0x400

#define DEMO_STATUS_IDLE         0x0
#define DEMO_STATUS_START        0x1 /* Status value to indicate demo start */

#define TEST_MSG "Hello World - libmetal shared memory demo (ZynQ.A53)"
#include <array>
#include <tuple>
#include <algorithm>

int shmem_echo( struct metal_io_region *shm_io)
{
    struct msg_hdr_s {
        uint32_t index;
        uint32_t len;
    };

    static const char *backtrace = __PRETTY_FUNCTION__ ;
    unsigned int data_len = sizeof(struct msg_hdr_s) + strlen(TEST_MSG) + 1;
    auto setup_protocol = [shm_io,data_len]()
    {
        metal_io_write32(shm_io, SHM_DEMO_CNTRL_OFFSET, 0);
        metal_io_write32(shm_io, SHM_TX_AVAIL_OFFSET, 0);
        metal_io_write32(shm_io, SHM_RX_AVAIL_OFFSET, 0);
        void *tx_data = metal_allocate_memory( data_len );
        void *rx_data = metal_allocate_memory( data_len );
        if( tx_data == nullptr || rx_data == nullptr )
            return std::make_tuple( tx_data, rx_data, -1 );
        struct msg_hdr_s *msg_hdr = (struct msg_hdr_s *)tx_data;
        msg_hdr->index = 0;
        msg_hdr->len = strlen(TEST_MSG) + 1;
        return std::make_tuple( tx_data, rx_data, 0 );
    };

    LPRINTF("Setting up protocol\n");
    auto [ tx_data, rx_data, err ] = setup_protocol();
    using buffers_t = std::array<void*,2>;
    auto release_memory = []( buffers_t buffers ) {
        std::for_each( begin(buffers), end(buffers), [](auto b){ metal_free_memory(b); } );
    };
    auto error = [&]( const char *descriptor, int err, buffers_t buffers )
    {
        LPERROR("%s ( failed ) : %20s (%d)", backtrace, descriptor, err);
        metal_io_write32( shm_io, SHM_DEMO_CNTRL_OFFSET, DEMO_STATUS_IDLE );
        release_memory( buffers );
        return err;
    };

    if( err < 0 )
        return error("Failed to allocate memory aborted\n", -1, { tx_data, rx_data } );

    char *ref = (char*)tx_data + sizeof( msg_hdr_s );
    sprintf( ref, TEST_MSG);

    LPRINTF("Starting shared memory demo\n");
    metal_io_write32(shm_io, SHM_DEMO_CNTRL_OFFSET, DEMO_STATUS_START);

    LPRINTF("Sending message: %s\n", ref );
    if ( metal_io_block_write( shm_io, SHM_TX_BUFFER_OFFSET, tx_data, data_len ) < 0)
        return error("Unable to metal_io_block_write()\n", -2, { tx_data, rx_data } );
    int tx_count = 0;
    metal_io_write32(shm_io, SHM_TX_AVAIL_OFFSET, ++tx_count);
    unsigned int rx_count = 0;
    while ( metal_io_read32(shm_io, SHM_RX_AVAIL_OFFSET) == rx_count);
    rx_count++;

    if ( metal_io_block_read( shm_io, SHM_RX_BUFFER_OFFSET, rx_data, data_len) < 0 )
        return error("Unable to metal_io_block_read()\n", -3, { tx_data, rx_data } );

    int ret = memcmp( tx_data, rx_data, data_len);
	if (ret) {
		LPERROR("Received data verification failed.\n");
		LPRINTF("Expected:");
		dump_buffer(tx_data, data_len);
		LPRINTF("Actual:");
		dump_buffer(rx_data, data_len);
    } else
    {
        LPRINTF("Message Received: %s\n", (char *)(rx_data) + sizeof(msg_hdr_s));
	}

    metal_io_write32( shm_io, SHM_DEMO_CNTRL_OFFSET, DEMO_STATUS_IDLE );
    release_memory ( { tx_data, rx_data } );
    LPRINTF("Shared memory demo: %s.\n", ret ? "Failed": "Passed" );
	return ret;

}

int shmem_demo()
{
    static const char *backtrace = __PRETTY_FUNCTION__ ;
    struct metal_device *device = nullptr;
    auto auditor = [&]( const char *descriptor, int err = 0 )
    {
        if( err < 0 )
        {
            LPERROR("%s : %20s (%d)\n", backtrace, descriptor, err);
            if( device )
                metal_device_close( device );
            return err;
        }
        LPRINTF("%s: %s\n", backtrace, descriptor );
        return 0;
    };

    auditor( __PRETTY_FUNCTION__ );
    int ret = metal_device_open( BUS_NAME, SHM_DEV_NAME, &device);
    if ( ret )
        return auditor( ( std::string("Failed to open device: ") +  ( SHM_DEV_NAME ) ).c_str(), -1 );

    struct metal_io_region *io = metal_device_io_region( device, 0 );
    if ( io == nullptr )
        return auditor( ( std::string( "Failed to get io region of device: ") + device->name ).c_str(), -ENODEV );

    ret = shmem_echo( io );
    metal_device_close(device);
	return ret;
}
