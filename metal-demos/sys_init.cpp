
/*
 * Copyright (c) 2016, Xilinx Inc. and Contributors. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>
#include <unistd.h>
#include <metal/sys.h>
#include "common.h"

extern "C" {
int sys_init()
{
    auto error = []( const char *backtrace ){ LPERROR("%s",backtrace ); return -1; };
	struct metal_init_params init_param = METAL_INIT_DEFAULTS;
    if ( int ret = metal_init( & init_param ); ret )
        return error( " Failed to initialize libmetal");
    return 0 ;
}

int sys_cleanup()
{
	metal_finish();
    return 0;
}

void wait_for_interrupt(void) {
	return;
}
}
