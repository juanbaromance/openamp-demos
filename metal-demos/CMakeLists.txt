set(pwd ${CMAKE_CURRENT_SOURCE_DIR})
include_directories ( ${pwd} ${pwd}/dependencies/include)
cmake_minimum_required(VERSION 3.13)

set ( CMAKE_SYSTEM_PROCESSOR "arm" CACHE STRING "")
set ( MACHINE        "zynqmp_a53" CACHE STRING "")
set ( CROSS_PREFIX   "aarch64-linux-gnu-" CACHE STRING "")
set ( CMAKE_C_COMPILER   ${CROSS_PREFIX}gcc-8 )
set ( CMAKE_CXX_COMPILER ${CROSS_PREFIX}g++-8 )

set ( _deps metal sysfs )
foreach ( _dep ${_deps} )
    add_library( ${_dep} SHARED IMPORTED )
endforeach (_dep)

set_property(TARGET metal PROPERTY IMPORTED_LOCATION ${pwd}/dependencies/lib/libmetal.so )
set_property(TARGET sysfs PROPERTY IMPORTED_LOCATION ${pwd}/dependencies/lib/libsysfs.so.2 )

set (_src_common ${pwd}/sys_init.cpp )
foreach (_app apu_main libmetal_amp_demod)
  set (_src ${pdw}/${_app}.cpp)
  list(APPEND _src ${_src_common})
  list(APPEND _src ${pwd}/shmem_demo.cpp )
  list(APPEND _src ${pwd}/shmem_atomic_demo.cpp)
  list(APPEND _src ${pwd}/ipi_shmem_demo.c)
  list(APPEND _src ${pwd}/ipi_latency_demo.c)
  list(APPEND _src ${pwd}/shmem_latency_demo.c)
  list(APPEND _src ${pwd}/shmem_throughput_demo.c)
  add_executable (${_app}.x ${pwd}/${_src})
  target_link_libraries (${_app}.x ${_deps} )
  target_compile_features(${_app}.x PRIVATE cxx_std_17)
endforeach (_app)

# vim: expandtab:ts=2:sw=2:smartindent

