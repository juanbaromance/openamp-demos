/*
 * Copyright (c) 2017, Xilinx Inc. and Contributors. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

 /*****************************************************************************
 * atomic_shmem_demod.c - Shared memory atomic operation demo
 * This demo will:
 *
 *  1. Open the shared memory device.
 *  2. Open the IPI device.
 *  3. Register IPI interrupt handler.
 *  4. Kick IPI to notify the other end to start the demo
 *  5. Start atomic add by 1 for 5000 times over the shared memory
 *  6. Wait for remote IPI kick to know when the remote has finished the demo.
 *  7. Verify the result. As two sides both have done 5000 times of adding 1,
 *     check if the end result is 5000*2.
 *  8. Clean up: deregister the IPI interrupt handler, close the IPI device
 *     , close the shared memory device.
 */

#include <unistd.h>
#include <stdio.h>
#include <metal/atomic.h>
#include <metal/cpu.h>
#include <metal/io.h>
#include <sys/types.h>
#include <metal/device.h>
#include <metal/irq.h>
#include <errno.h>
#include <time.h>
#include "common.h"

#define ATOMIC_INT_OFFSET 0x0 /* shared memory offset for atomic operation */
#define ITERATIONS 5000

/* is remote kicked, 0 - kicked, 1 - not-kicked */
static atomic_int remote_nkicked;

static int ipi_irq_handler( int vect_id, void *priv )
{
    (void)vect_id;
    struct metal_io_region *ipi_io = (struct metal_io_region *)priv;
    if ( ipi_io == nullptr )
        return METAL_IRQ_NOT_HANDLED;

    constexpr const uint32_t ipi_mask = IPI_MASK;
    if ( metal_io_read32( ipi_io, IPI_ISR_OFFSET) & ipi_mask )
    {
        metal_io_write32( ipi_io, IPI_ISR_OFFSET, ipi_mask);
        atomic_flag_clear( & remote_nkicked );
		return METAL_IRQ_HANDLED;
	}
	return METAL_IRQ_NOT_HANDLED;
}

static int master_shmem(struct metal_io_region *ipi_io, struct metal_io_region *shm_io)
{
    constexpr const char *backtrace = __FUNCTION__ ;
    constexpr uint32_t ipi_mask = IPI_MASK;
    atomic_int *shm_int = (atomic_int *)metal_io_virt(shm_io, ATOMIC_INT_OFFSET);
    atomic_store(shm_int, 0);
    LPRINTF("%s : battle with 0x%08x : starts with %d -> rp.kicked\n", backtrace, ipi_mask, *shm_int );
    metal_io_write32( ipi_io, IPI_TRIG_OFFSET, ipi_mask);
    atomic_signal_fence(memory_order_acq_rel);

    for (int i = 0; i < ITERATIONS; i++)
        atomic_fetch_add(shm_int, 1);
    wait_for_notified( & remote_nkicked );

    LPRINTF("%s : expected state: %u, actual: %u\n",
            backtrace, (unsigned int)(ITERATIONS << 1), *shm_int );
    return atomic_load(shm_int) == (ITERATIONS << 1 ) ? 0 : -1;
}

#include <sstream>
#include <iostream>
#include <array>
#include <bitset>
constexpr static uint32_t rpu_mask = IPI_MASK;
int atomic_shmem_demo()
{
    using metal_device = struct metal_device;
    metal_device *ipi_dev = NULL, *shm_dev = NULL;
	struct metal_io_region *ipi_io = NULL, *shm_io = NULL;
	int ipi_irq;
	int ret = 0;


    static const char *backtrace = __PRETTY_FUNCTION__ ;
    auto release_resources = [&](){
        std::array<metal_device*,2> devices = { ipi_dev, shm_dev };
        for( auto & d : devices )
            if( d )
                metal_device_close( d );
    };

    auto auditor = [&]( const char *descriptor, int err = 0 )
    {
        if( err < 0 )
        {
            release_resources();
            LPERROR("%s : %20s (%d)\n", backtrace, descriptor, err);
            return err;
        }
        LPRINTF("%s: %s\n", backtrace, descriptor );
        return 0;
    };

    auto ipi_cleanup = [](metal_io_region * ior, const int irq, const uint32_t mask = rpu_mask )
    {
        metal_io_write32(ior, IPI_IDR_OFFSET, mask);
        metal_irq_disable(irq);
        metal_irq_unregister(irq);
    };

    auto ipi_setup = []( metal_io_region * ior, const int irq, const uint32_t mask = rpu_mask  )
    {
        metal_io_write32( ior, IPI_IDR_OFFSET, rpu_mask );
        metal_io_write32( ior, IPI_ISR_OFFSET, rpu_mask );
        if( metal_irq_register( irq, ipi_irq_handler, ior ) < 0 )
            return -EACCES;
        metal_irq_enable(irq);
        atomic_init( & remote_nkicked, 1 );
        metal_io_write32( ior, IPI_IER_OFFSET, rpu_mask );
        return 0;
    };

    auditor("atomic operation over shared memory");

    if( metal_device_open(BUS_NAME, SHM_DEV_NAME, &shm_dev))
        return auditor( ( std::string("Failed to open for") + SHM_DEV_NAME ).c_str(),  -ENODEV);
    if ( ( shm_io = metal_device_io_region(shm_dev, 0) ) == nullptr )
        return auditor( ( std::string("Failed to map io region for") + shm_dev->name ).c_str(), -ENODEV);

    if (metal_device_open(BUS_NAME, IPI_DEV_NAME, &ipi_dev))
        return auditor( ( std::string("Failed to open for") + IPI_DEV_NAME ).c_str(),  -ENODEV);
    if ( ( ipi_io = metal_device_io_region(ipi_dev, 0) ) == nullptr )
        return auditor( ( std::string("Failed to map io region for") + ipi_dev->name ).c_str(),  -ENODEV);

    ipi_irq = (intptr_t)ipi_dev->irq_info;
    if( ( ret = ipi_setup(ipi_io, ipi_irq ) ) < 0 )
        return auditor("metal-irq-setup failured:", ret );

    std::ostringstream oss;
    oss << "ipi." << std::hex <<  metal_io_phys( ipi_io, 0 )
        << ".irq(" << ipi_irq << ")(" << ret << ")"
        << " -> 0x" << std::bitset<32>(IPI_MASK).to_ulong();
    auditor( oss.str().c_str() );   

    ret = master_shmem( ipi_io, shm_io );

    ipi_cleanup( ipi_io, ipi_irq );
    return auditor("Done");

}
